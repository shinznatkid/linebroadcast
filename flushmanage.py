#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    fileno = sys.stdout.fileno()
    new_stdout = os.fdopen(fileno, 'w', 0)
    sys.stdout = new_stdout  # autoflush

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "linedistribute.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
