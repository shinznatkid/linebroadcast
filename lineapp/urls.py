from django.conf.urls import patterns, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    'lineapp.views',
    url(r'^$', 'index', name='index'),
    url(r'^contacts/$', 'contacts', name='contacts'),

    url(r'^upload_image/$', 'upload_image', name='upload_image'),
    url(r'^chat/(?P<lineid>\w+)/$', 'chat', name='chat'),
    url(r'^chat/(?P<lineid>\w+)/fetch_chat/$', 'ajax_fetch_chat', name='ajax_fetch_chat'),
    url(r'^chat/(?P<lineid>\w+)/fetch_history_chat/$', 'ajax_fetch_history_chat', name='ajax_fetch_history_chat'),
    url(r'^chat/(?P<lineid>\w+)/read_chat/$', 'ajax_mark_read_chat', name='ajax_mark_read_chat'),
    url(r'^chat/(?P<lineid>\w+)/send_message/$', 'ajax_send_message', name='ajax_send_message'),
    url(r'^chat/(?P<lineid>\w+)/close/$', 'ajax_close_chat', name='ajax_close_chat'),
    url(r'^blocked/$', 'blocked', name='blocked'),

    url(r'^add/ajax/find_contact_by_userid/$', 'ajax_find_contact_by_userid', name='ajax_find_contact_by_userid'),
    url(r'^add/ajax/add_contact_by_userid/$', 'ajax_add_contact_by_userid', name='ajax_add_contact_by_userid'),

    url(r'^api/statistic/$', 'api_statistic', name='api_statistic'),
)
