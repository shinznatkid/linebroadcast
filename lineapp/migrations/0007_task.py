# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0006_auto_20150406_2306'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('action', models.CharField(max_length=50, choices=[('get_messages', 'Get Messages')])),
                ('args', jsonfield.fields.JSONField(default=dict)),
                ('output', jsonfield.fields.JSONField(default=dict)),
                ('completed', models.BooleanField(default=False)),
                ('completed_on', models.DateTimeField(null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('account', models.ForeignKey(related_name='tasks', to='lineapp.Account')),
            ],
            options={
                'verbose_name': 'Task',
                'verbose_name_plural': 'Tasks',
            },
            bases=(models.Model,),
        ),
    ]
