# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0002_auto_20150326_0642'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='is_joined',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
