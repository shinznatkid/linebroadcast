# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0038_auto_20151118_1949'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='state_message',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
