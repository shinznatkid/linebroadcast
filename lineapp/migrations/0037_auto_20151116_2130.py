# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0036_auto_20151116_2046'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='pincode',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='account',
            name='state',
            field=models.IntegerField(default=0),
        ),
    ]
