# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0003_auto_20150326_0642'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='picture_status',
            field=models.CharField(max_length=64, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='group',
            name='picture_status',
            field=models.CharField(max_length=64, null=True),
            preserve_default=True,
        ),
    ]
