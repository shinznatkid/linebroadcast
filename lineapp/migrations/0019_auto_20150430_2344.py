# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0018_message_link_room'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='output',
            field=jsonfield.fields.JSONField(default=dict),
            preserve_default=True,
        ),
    ]
