# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0041_account_certificate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='lineid',
            field=models.CharField(max_length=33, db_index=True),
        ),
    ]
