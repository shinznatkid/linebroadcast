# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0019_auto_20150430_2344'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='status_message',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
    ]
