# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0013_auto_20150425_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='account',
            field=models.ForeignKey(related_name='contacts', blank=True, to='lineapp.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='worked_by',
            field=models.ForeignKey(related_name='worked_contacts', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='group',
            name='account',
            field=models.ForeignKey(related_name='groups', blank=True, to='lineapp.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='group',
            name='worked_by',
            field=models.ForeignKey(related_name='worked_groups', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
