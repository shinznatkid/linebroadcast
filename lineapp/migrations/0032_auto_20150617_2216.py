# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import lineapp.models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0031_auto_20150531_1503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imageattachment',
            name='image',
            field=models.ImageField(upload_to=lineapp.models.content_file_name),
            preserve_default=True,
        ),
    ]
