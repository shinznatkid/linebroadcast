# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0034_auto_20150805_2034'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='access_token',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='group',
            name='access_token',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='room',
            name='access_token',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
    ]
