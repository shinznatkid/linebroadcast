# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lineid', models.CharField(unique=True, max_length=33, db_index=True)),
                ('name', models.CharField(max_length=255)),
                ('is_joined', models.BooleanField()),
                ('creator', models.ForeignKey(related_name='own_groups', to='lineapp.Contact', null=True)),
                ('invitee', models.ManyToManyField(related_name='invitee_groups', to='lineapp.Contact')),
                ('members', models.ManyToManyField(related_name='groups', to='lineapp.Contact')),
            ],
            options={
                'verbose_name': 'Group',
                'verbose_name_plural': 'Groups',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lineid', models.CharField(unique=True, max_length=33, db_index=True)),
                ('text', models.TextField(null=True, blank=True)),
                ('channel', models.PositiveIntegerField(choices=[(0, 'User'), (1, 'Room'), (2, 'Group')])),
                ('sender_id', models.CharField(db_index=True, max_length=33, null=True, blank=True)),
                ('receiver_id', models.CharField(db_index=True, max_length=33, null=True, blank=True)),
                ('created_on', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Message',
                'verbose_name_plural': 'Messages',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='account',
            name='profile',
            field=models.ForeignKey(blank=True, to='lineapp.Contact', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='lineid',
            field=models.CharField(unique=True, max_length=33, db_index=True),
            preserve_default=True,
        ),
    ]
