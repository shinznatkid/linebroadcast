# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0011_auto_20150421_1721'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='revision',
            field=models.PositiveIntegerField(default=None, null=True, blank=True),
            preserve_default=True,
        ),
    ]
