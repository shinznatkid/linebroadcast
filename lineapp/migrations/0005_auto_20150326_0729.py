# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0004_auto_20150326_0721'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='status_message',
            field=models.CharField(max_length=255, null=True),
            preserve_default=True,
        ),
    ]
