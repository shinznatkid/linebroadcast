# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0028_imageattachment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imageattachment',
            name='image',
            field=models.ImageField(upload_to='attachments/'),
        ),
        migrations.AlterField(
            model_name='task',
            name='action',
            field=models.CharField(max_length=50, choices=[('get_messages', 'Get Messages'), ('send_message', 'Send Message'), ('send_image', 'Send Image'), ('find_contact_by_userid', 'Find contact by userid'), ('add_contact_by_userid', 'Add contact by userid'), ('get_blocked_contacts', 'Get blocked contacts')]),
        ),
    ]
