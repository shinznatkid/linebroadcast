# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lineapp', '0016_auto_20150426_1536'),
    ]

    operations = [
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lineid', models.CharField(unique=True, max_length=33, db_index=True)),
                ('worked_on', models.DateTimeField(null=True, blank=True)),
                ('message_updated_on', models.DateTimeField(null=True, blank=True)),
                ('account', models.ForeignKey(related_name='rooms', blank=True, to='lineapp.Account', null=True)),
                ('contacts', models.ManyToManyField(related_name='rooms', to='lineapp.Contact')),
                ('latest_message', models.ForeignKey(related_name='rooms', blank=True, to='lineapp.Message', null=True)),
                ('worked_by', models.ForeignKey(related_name='worked_rooms', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Room',
                'verbose_name_plural': 'Rooms',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='account',
            name='revision',
            field=models.PositiveIntegerField(default=None, help_text='Optional', null=True, blank=True),
            preserve_default=True,
        ),
    ]
