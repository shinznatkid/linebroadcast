# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lineapp', '0009_message_read_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='worked_by',
            field=models.ForeignKey(related_name='line_contacts', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contact',
            name='worked_on',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='group',
            name='worked_by',
            field=models.ForeignKey(related_name='line_groups', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='group',
            name='worked_on',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
