# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lineapp', '0035_auto_20151116_2004'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='created_by',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='account',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 16, 20, 46, 9, 130000), auto_now_add=True),
            preserve_default=False,
        ),
    ]
