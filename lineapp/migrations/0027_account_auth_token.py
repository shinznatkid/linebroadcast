# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0026_auto_20150505_0215'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='auth_token',
            field=models.TextField(default=None, null=True, blank=True),
        ),
    ]
