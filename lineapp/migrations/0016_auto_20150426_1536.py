# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0015_auto_20150426_1439'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='action',
            field=models.CharField(max_length=50, choices=[('get_messages', 'Get Messages'), ('send_message', 'Send Message'), ('find_contact_by_userid', 'Find contact by userid'), ('add_contact_by_userid', 'Add contact by userid')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='task',
            name='output',
            field=jsonfield.fields.JSONField(default=dict),
            preserve_default=True,
        ),
    ]
