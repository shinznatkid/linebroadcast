# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lineapp', '0033_auto_20150703_1536'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='latest_worked_by',
            field=models.ForeignKey(related_name='latest_worked_contacts', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='latest_worked_by',
            field=models.ForeignKey(related_name='latest_worked_groups', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='room',
            name='latest_worked_by',
            field=models.ForeignKey(related_name='latest_worked_rooms', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
