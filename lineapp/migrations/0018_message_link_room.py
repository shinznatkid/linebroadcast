# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0017_auto_20150429_1414'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='link_room',
            field=models.ForeignKey(related_name='messages', blank=True, to='lineapp.Room', null=True),
            preserve_default=True,
        ),
    ]
