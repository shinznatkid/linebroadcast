# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0010_auto_20150421_1502'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='latest_message',
            field=models.ForeignKey(related_name='contacts', blank=True, to='lineapp.Message', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='group',
            name='latest_message',
            field=models.ForeignKey(related_name='groups', blank=True, to='lineapp.Message', null=True),
            preserve_default=True,
        ),
    ]
