# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django_enumfield import enum
import datetime
import random
import string
import time

from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
from django.db.models import Q, Prefetch
from django.db import models, transaction
from jsonfield import JSONField
from django.utils import timezone
from django.utils.timesince import timesince
from utils.misc import minimal_timesince
from utils.redisqueue import RedisQueue


class AccountState(enum.Enum):
    UNVERIFY     = 0
    VERIFING     = 1
    VERIFIED     = 2
    WAIT_PINCODE = 10


@python_2_unicode_compatible
class Account(models.Model):

    class Meta:
        verbose_name = "Account"
        verbose_name_plural = "Accounts"

    def __str__(self):
        if self.nickname:
            return self.nickname
        return self.email

    nickname    = models.CharField(max_length=255, blank=True, null=True)
    email       = models.CharField(max_length=50)  # Maybe LineID Instead
    password    = models.CharField(max_length=50)
    profile     = models.OneToOneField('Contact', null=True, blank=True, related_name='%(class)s_account')
    revision    = models.PositiveIntegerField(help_text='Optional', null=True, blank=True, default=None)
    updated_on  = models.DateTimeField(null=True, blank=True, default=None)
    auth_token  = models.TextField(blank=True, null=True, default=None)
    certificate = models.TextField(blank=True)

    created_on    = models.DateTimeField(auto_now_add=True)
    created_by    = models.ForeignKey(User)
    state         = enum.EnumField(AccountState, default=AccountState.UNVERIFY)
    state_message = models.CharField(max_length=255, blank=True, null=True)
    pincode       = models.CharField(max_length=50, blank=True, null=True)

    def get_lineid(self, lineid):
        contact = Contact.objects.filter(account=self, lineid=lineid).first()
        if contact:
            return contact
        group = Group.objects.filter(account=self, lineid=lineid).first()
        if group:
            return group
        room = Room.objects.filter(account=self, lineid=lineid).first()
        if room:
            return room
        return None

    def update_line(self):
        if not self.updated_on or timezone.now() > self.updated_on + datetime.timedelta(seconds=10):  # save after more than 10 for optimized
            self.updated_on = timezone.now()
            self.save(update_fields=['updated_on'])

    @property
    def is_online(self):
        if self.updated_on:
            return timezone.now() < self.updated_on + datetime.timedelta(seconds=30)
        return False

    @property
    def status(self):
        if self.state == AccountState.VERIFIED:
            return bool(self.auth_token)
        else:
            return False

    def is_verified(self):
        return self.state == AccountState.VERIFIED

    def verify(self):
        login_history = LoginHistory(account=self)
        login_history.save()
        self.state = AccountState.VERIFING
        self.save(update_fields=['state'])
        queue = RedisQueue('linebroadcast')
        queue.put(['verify_account', self.pk, login_history.pk])

    def reload(self):
        account = Account.objects.get(pk=self.pk)
        self.__dict__.update(account.__dict__)
        return account


@python_2_unicode_compatible
class LoginHistory(models.Model):

    def __str__(self):
        return str(self.account)

    account       = models.ForeignKey(Account, related_name='login_histories')
    state         = enum.EnumField(AccountState, default=AccountState.VERIFING)
    state_message = models.CharField(max_length=255, blank=True, null=True)
    pincode       = models.CharField(max_length=50, blank=True, null=True)
    created_on    = models.DateTimeField(auto_now_add=True)
    updated_on    = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        super(LoginHistory, self).save(*args, **kwargs)
        self.account.state = self.state
        self.account.state_message = self.state_message
        self.account.pincode = self.pincode
        self.account.save(update_fields=['state', 'state_message', 'pincode'])


@python_2_unicode_compatible
class Task(models.Model):

    ACTION_GET_MESSAGES           = 'get_messages'
    ACTION_SEND_MESSAGE           = 'send_message'
    ACTION_READ_MESSAGE           = 'read_message'
    ACTION_SEND_IMAGE             = 'send_image'
    ACTION_FIND_CONTACT_BY_USERID = 'find_contact_by_userid'
    ACTION_ADD_CONTACT_BY_USERID  = 'add_contact_by_userid'
    ACTION_GET_BLOCKED_CONTACTS   = 'get_blocked_contacts'

    ACTION_CHOICES = (
        (ACTION_GET_MESSAGES, 'Get Messages'),
        (ACTION_SEND_MESSAGE, 'Send Message'),
        (ACTION_READ_MESSAGE, 'Read Message'),
        (ACTION_SEND_IMAGE, 'Send Image'),
        (ACTION_FIND_CONTACT_BY_USERID, 'Find contact by userid'),
        (ACTION_ADD_CONTACT_BY_USERID, 'Add contact by userid'),
        (ACTION_GET_BLOCKED_CONTACTS, 'Get blocked contacts'),
    )

    class Meta:
        verbose_name        = "Task"
        verbose_name_plural = "Tasks"

    def __str__(self):
        return '%s: %s(%s)' % (self.account, self.action, self.args)

    account      = models.ForeignKey(Account, related_name='tasks')
    action       = models.CharField(choices=ACTION_CHOICES, max_length=50)
    args         = JSONField()
    completed    = models.BooleanField(default=False)
    completed_on = models.DateTimeField(null=True, blank=True)
    created_on   = models.DateTimeField(auto_now_add=True)
    output       = JSONField()

    def set_complete(self, result={}):
        self.completed = True
        self.completed_on = timezone.now()
        if not result:
            result = {}
        self.output = result
        self.save()

    def wait(self):
        timeout      = 30
        time_sleep   = 0.5
        time_counter = 0
        while time_counter <= timeout:
            self.reload()
            if self.completed:
                return True
            time.sleep(time_sleep)
            time_counter += time_sleep
        return False

    def reload(self):
        new_self = self.__class__.objects.get(pk=self.pk)
        self.__dict__.update(new_self.__dict__)


class AbstractBaseContact(models.Model):

    class Meta:
        abstract = True

    def __str__(self):
        return self.lineid

    account          = models.ForeignKey(Account, related_name='%(class)ss', null=True, blank=True)
    lineid           = models.CharField(max_length=33, db_index=True)  # Length = 33
    worked_by        = models.ForeignKey(User, related_name='worked_%(class)ss', null=True, blank=True)  # User open chat
    latest_worked_by = models.ForeignKey(User, related_name='latest_worked_%(class)ss', null=True, blank=True)  # User open chat
    worked_on        = models.DateTimeField(null=True, blank=True)  # User open chat/fished job
    access_token     = models.CharField(max_length=256, null=True, blank=True)

    @property
    def unread_messages(self):
        if not hasattr(self, '_unread_messages'):
            self._unread_messages = self.messages.filter(read_by__isnull=True)
        return self._unread_messages

    @property
    def is_working(self):
        max_idle_time = 5 * 60  # 5 min
        if self.worked_by_id and self.worked_on:
            if datetime.datetime.now() < self.worked_on + datetime.timedelta(seconds=max_idle_time):  # In time
                return True
        return False

    @property
    def is_worked_expired(self):
        max_idle_time = 5 * 60  # 5 min
        if self.worked_by_id and self.worked_on:
            if datetime.datetime.now() < self.worked_on + datetime.timedelta(seconds=max_idle_time):  # In time
                return False
            return True
        return False

    @property
    def working_time(self):
        if self.worked_on:
            work_time = datetime.datetime.now() - self.worked_on
            return work_time
        return False

    def get_messages(self, latest_message_id=None, timeout=0, limit=None, check_update_on_read=False):

        def make_queries():
            message_query = self.messages
            if latest_message_id:
                message_query = message_query.filter(pk__gt=latest_message_id)
            if limit:
                _messages = message_query.order_by('-created_on').all()
                _messages = _messages[:limit][::-1]
            else:
                _messages = message_query.order_by('created_on').all()
            return _messages

        # Use Limit only when no timeout or have queries
        latest_line_read_message_id = self.latest_line_read_message_id
        messages = make_queries()
        if messages or not timeout:
            return messages
        # if have timeout
        counter = 0
        sleeptime = 0.5
        while counter <= timeout:
            messages = list(make_queries())
            if messages:
                return messages
            self.reload()
            if self.latest_line_read_message_id != latest_line_read_message_id:  # update
                return []
            counter += sleeptime
            time.sleep(sleeptime)
        return []

    def get_history_messages(self, first_message_id, limit=None):
        '''
        Return as [newest -> oldest]
        '''

        messages = self.messages.filter(pk__lt=first_message_id).order_by('-created_on').all()
        if limit:
            messages = messages[:limit]
        # messages = messages[::-1]
        return list(messages)

    def serialize(self):
        latest_message_time = None
        if self.latest_message and self.latest_message.created_on:
            latest_message_time = self.latest_message.created_on.strftime('%d/%m/%y %H:%M')
        worked_on        = minimal_timesince(self.worked_on)
        # worked_on        = timesince(self.worked_on) if self.worked_on else None
        worked_by        = str(self.worked_by) if self.worked_by else None
        latest_worked_by = str(self.latest_worked_by) if self.latest_worked_by else None
        worked_by_id     = self.worked_by_id
        return {
            'is_working': self.is_working,
            'worked_by': worked_by,
            'worked_by_id': worked_by_id,
            'worked_on': worked_on,
            'latest_worked_by': latest_worked_by,
            'is_worked_expired': self.is_worked_expired,
            'lineid': self.lineid,
            'name': str(self),
            'profile_picture_url': self.profile_picture_url(),
            'latest_message': latest_message_time,
            'unread_messages_count': len(self.unread_messages),
        }

    def reload(self):
        new_self = self.__class__.objects.get(pk=self.pk)
        self.__dict__.update(new_self.__dict__)

    def generate_token(self, force=False):
        if not self.access_token or force:
            n = 64
            self.access_token = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(n))
            self.save(update_fields=['access_token'])
        return self.access_token


class ContactQuerySet(models.QuerySet):
    def prefetch_unread_messages(self):
        return self.prefetch_related(Prefetch("messages", queryset=Message.objects.filter(read_by__isnull=True), to_attr="_unread_messages"))


@python_2_unicode_compatible
class Contact(AbstractBaseContact):

    class Meta:
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"

    def __str__(self):
        return u'%s - %s' % (self.account, self.name)

    name                     = models.CharField(max_length=255)
    status_message           = models.TextField(null=True)
    picture_status           = models.CharField(max_length=64, null=True)
    message_updated_on       = models.DateTimeField(null=True, blank=True)
    latest_message           = models.ForeignKey('Message', related_name='contacts', null=True, blank=True)
    latest_line_read_message = models.ForeignKey('Message', related_name='line_read_contacts', null=True, blank=True)

    objects = ContactQuerySet.as_manager()

    @property
    def unread_messages(self):
        if not hasattr(self, '_unread_messages'):
            self._unread_messages = self.messages.filter(read_by__isnull=True)
        return self._unread_messages

    def profile_picture_url(self, size='small'):
        '''
        Profile picture any size (None, small, large)
        '''
        return 'http://dl.profile.line.naver.jp/os/p/%s/%s' % (self.lineid, size)

    def get_latest_message(self):
        profile = self.account.profile
        assert profile is not None
        account_lineid = profile.lineid
        query_chat = (Q(sender_id=self.lineid, receiver_id=account_lineid) | Q(sender_id=account_lineid, receiver_id=self.lineid))
        return Message.objects.filter(query_chat, channel=Message.USER).order_by('created_on').last()


class GroupQuerySet(models.QuerySet):
    def prefetch_unread_messages(self):
        return self.prefetch_related(Prefetch("messages", queryset=Message.objects.filter(read_by__isnull=True), to_attr="_unread_messages"))


@python_2_unicode_compatible
class Group(AbstractBaseContact):

    class Meta:
        verbose_name = "Group"
        verbose_name_plural = "Groups"

    def __str__(self):
        return u'%s - %s' % (self.account, self.name)

    name                     = models.CharField(max_length=255)
    is_joined                = models.BooleanField(default=True)
    creator                  = models.ForeignKey(Contact, related_name='own_groups', null=True)
    members                  = models.ManyToManyField(Contact, related_name='groups')
    invitee                  = models.ManyToManyField(Contact, related_name='invitee_groups')
    picture_status           = models.CharField(max_length=64, null=True)
    message_updated_on       = models.DateTimeField(null=True, blank=True)
    latest_message           = models.ForeignKey('Message', related_name='groups', null=True, blank=True)
    latest_line_read_message = models.ForeignKey('Message', related_name='line_read_groups', null=True, blank=True)

    objects = GroupQuerySet.as_manager()

    def profile_picture_url(self, size='small'):
        '''
        Profile (Group) picture any size (None, small, large)
        '''
        return 'http://dl.profile.line.naver.jp/%s/%s' % (self.picture_status, size)


class RoomQuerySet(models.QuerySet):
    def prefetch_unread_messages(self):
        return self.prefetch_related(Prefetch("messages", queryset=Message.objects.filter(read_by__isnull=True), to_attr="_unread_messages"))


@python_2_unicode_compatible
class Room(AbstractBaseContact):

    class Meta:
        verbose_name = "Room"
        verbose_name_plural = "Rooms"

    def __str__(self):
        return ', '.join([str(x) for x in self.contacts.all()])

    contacts                 = models.ManyToManyField(Contact, related_name='rooms')
    message_updated_on       = models.DateTimeField(null=True, blank=True)
    latest_message           = models.ForeignKey('Message', related_name='rooms', null=True, blank=True)
    latest_line_read_message = models.ForeignKey('Message', related_name='line_read_rooms', null=True, blank=True)

    objects = RoomQuerySet.as_manager()

    def profile_picture_url(self, size='small'):
        '''
        Profile (Group) picture any size (None, small, large)
        '''
        return '/static/images/chat_room.png'


@python_2_unicode_compatible
class Message(models.Model):

    USER  = 0
    ROOM  = 1
    GROUP = 2

    CHANNEL_CHOICES = (
        (USER, 'User'),
        (ROOM, 'Room'),
        (GROUP, 'Group'),
    )

    CONTENT_TEXT  = 0
    CONTENT_IMAGE = 1
    CONTENT_STICKER = 2

    CONTENT_TYPE_CHOICES = (
        (CONTENT_TEXT, 'Text'),
        (CONTENT_IMAGE, 'Image'),
        (CONTENT_STICKER, 'Sticker'),
    )

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"

    def __str__(self):
        if self.text:
            return self.text
        else:
            return ''

    account         = models.ForeignKey(Account, related_name='%(class)s_message')
    lineid          = models.CharField(max_length=33, db_index=True)
    text            = models.TextField(blank=True, null=True)
    channel         = models.PositiveIntegerField(choices=CHANNEL_CHOICES)
    content_type    = models.PositiveIntegerField(choices=CONTENT_TYPE_CHOICES, default=CONTENT_TEXT)
    content_preview = models.ImageField(null=True, blank=True, upload_to='preview')
    content         = models.FileField(null=True, blank=True, upload_to='files')
    sender_id       = models.CharField(max_length=33, blank=True, null=True, db_index=True)
    receiver_id     = models.CharField(max_length=33, blank=True, null=True, db_index=True)
    line_read       = models.BooleanField(default=False)

    link_contact    = models.ForeignKey(Contact, blank=True, null=True, related_name='messages')
    link_group      = models.ForeignKey(Group, blank=True, null=True, related_name='messages')
    link_room       = models.ForeignKey(Room, blank=True, null=True, related_name='messages')

    created_on      = models.DateTimeField(db_index=True)
    read_by         = models.ForeignKey(User, related_name='messages', blank=True, null=True)

    @staticmethod
    def unread_messages(entity):
        if isinstance(entity, Contact):
            query_set_or = (Q(sender_id=entity.lineid) | Q(receiver_id=entity.lineid))
            query = Message.objects.filter(query_set_or, channel=Message.USER)
        elif isinstance(entity, Group):
            query = Message.objects.filter(channel=Message.GROUP, receiver_id=entity.lineid)
        else:
            raise NotImplementedError()
        return query.filter(read_by__isnull=True)

    def sender(self):
        return Contact.objects.filter(lineid=self.sender_id).first()

    def receiver(self):
        return Contact.objects.filter(lineid=self.receiver_id).first()

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        is_add = False
        if not self.pk:  # Insert
            is_add = True
        else:  # Update
            pass
        super(Message, self).save(force_insert, force_update, *args, **kwargs)
        # tag can add after save
        if is_add:  # Insert
            # lineid = self.sender_id
            # if not lineid:
            lineid = self.receiver_id  # use Group, Room instead
            if self.account.profile.lineid == lineid:  # If A send to me, use A!
                lineid = self.sender_id
            contact = self.account.get_lineid(lineid)
            if contact:
                contact.message_updated_on = self.created_on
                contact.latest_message = self
                contact.save(update_fields=['message_updated_on', 'latest_message'])

    def mark_read(self):  # Mark as Read in Client
        if not self.line_read:
            self.line_read = True
            self.save(update_fields=['line_read'])

            lineid = self.receiver_id  # use Group, Room instead
            if self.account.profile.lineid == lineid:  # If A send to me, use A!
                lineid = self.sender_id
            contact = self.account.get_lineid(lineid)
            if contact:
                if not contact.latest_line_read_message or contact.latest_line_read_message.pk < self.pk:
                    contact.latest_line_read_message = self
                    contact.save(update_fields=['latest_line_read_message'])
            return True
        return False

    def send_read(self):  # Mark as Read in Server
        if not self.line_read:
            self.line_read = True
            self.save(update_fields=['line_read'])
        link_entity = self.link_entity
        if link_entity:
            data_dict = {
                'lineid': link_entity.lineid,
                'message_id': self.lineid,
            }
            task = Task(account=self.account, action=Task.ACTION_READ_MESSAGE, args=data_dict)
            task.save()

    @property
    def link_entity(self):
        return self.link_contact or self.link_group or self.link_room

    def admin_thumbnail(self):
        if self.content_preview:
            return u'<img src="%s" />' % (self.content_preview.url)
        return ''
    admin_thumbnail.short_description = 'Thumbnail'
    admin_thumbnail.allow_tags = True


def content_file_name(instance, filename):
    return '/'.join(['attachments', 'file.jpg'])


class ImageAttachment(models.Model):

    class Meta:
        verbose_name = "Image Attachment"
        verbose_name_plural = "Image Attachments"

    def __str__(self):
        return str(self.image)

    image = models.ImageField(upload_to=content_file_name)


# Patch LineAPI Model
from lineapi.models import LineContact, LineGroup, LineRoom, LineMessage


def to_contact_model(self, account):
    contact = Contact.objects.filter(lineid=self.id, account=account).first()
    status_message = self.statusMessage.encode('tis-620', errors='ignore').decode('tis-620') if self.statusMessage else None
    name = self.name.encode('tis-620', errors='ignore').decode('tis-620') if self.name else None
    if not contact:
        contact = Contact(
            lineid=self.id,
            account=account,
            name=name,
            status_message=status_message,
            picture_status=self.pictureStatus,
        )
        contact.save()
    else:
        is_modify = False
        if contact.name != name:
            contact.name = name  # Update
            is_modify = True
        if contact.status_message != status_message:
            contact.status_message = status_message
            is_modify = True
        if contact.picture_status != self.pictureStatus:
            contact.picture_status = self.pictureStatus
            is_modify = True
        if is_modify:
            contact.save()
    return contact
LineContact.to_model = to_contact_model


def to_group_model(self, account, force_update_member=False):
    group = Group.objects.filter(lineid=self.id, account=account).first()
    name = self.name.encode('tis-620', errors='ignore').decode('tis-620') if self.name else None
    if not group:
        creator = self.creator.to_model(account) if self.creator else None
        group = Group(
            lineid=self.id,
            account=account,
            name=name,
            is_joined=self.is_joined,
            creator=creator,
            picture_status=self.pictureStatus,
        )
        group.save()
        # Update members, invitee just first time only
        group.members = [x.to_model(account) for x in self.members]
        group.invitee = [x.to_model(account) for x in self.invitee]
    else:
        is_modify = False
        if group.name != name:
            group.name           = name  # Update
            is_modify = True
        if group.is_joined != self.is_joined:
            group.is_joined      = self.is_joined
            is_modify = True
        if group.picture_status != self.pictureStatus:
            group.picture_status = self.pictureStatus
            is_modify = True
        if is_modify:
            group.save()
        if force_update_member:
            group.members = [x.to_model(account) for x in self.members]
            group.invitee = [x.to_model(account) for x in self.invitee]
    return group
LineGroup.to_model = to_group_model


def to_room_model(self, account, force_update_member=False):
    room = Room.objects.filter(lineid=self.id, account=account).first()
    if not room:
        room = Room(
            lineid=self.id,
            account=account,
        )
        room.save()
        # Update contacts, invitee just first time only
        room.contacts = [x.to_model(account) for x in self.contacts]
    else:
        is_modify = False
        if is_modify:
            room.save()
        if force_update_member:
            room.contacts = [x.to_model(account) for x in self.contacts]
    return room
LineRoom.to_model = to_room_model


def to_message_model(self, account):
    from curve.ttypes import ContentType
    from django.core.files import File
    # from io import BytesIO
    message = Message.objects.filter(lineid=self.id).first()
    if not message:
        if self.text:
            text     = self.text.encode('tis-620', errors='ignore').decode('tis-620') if self.text else None
        else:
            text     = None
        content_type = Message.CONTENT_TEXT
        content_preview = None
        content = None
        if self.contentType == ContentType.IMAGE:
            content_type = Message.CONTENT_IMAGE
            content_data = self.get_content()
            if content_data:
                content = 'temp/file_%s.jpg' % self.id
                with open(content, 'wb') as f:
                    f.write(content_data)
            if self.contentPreview:
                content_preview = 'temp/preview_%s.jpg' % self.id
                with open(content_preview, 'wb') as f:
                    f.write(self.contentPreview)
            else:
                content_preview = content  # No preview
        elif self.contentType == ContentType.STICKER:
            sticker_ver        = self.contentMetadata['STKVER']
            sticker_package_id = self.contentMetadata['STKPKGID']
            sticker_id         = self.contentMetadata['STKID']
            text               = 'http://dl.stickershop.line.naver.jp/products/0/0/%s/%s/PC/stickers/%s.png' % (sticker_ver, sticker_package_id, sticker_id)
            content_type       = Message.CONTENT_STICKER
        sender_id    = self.sender_id
        receiver_id  = self.receiver_id
        link_contact = None
        link_group   = None
        link_room    = None

        profile = account.profile
        assert profile is not None
        account_lineid = profile.lineid
        if self.sender_id == account_lineid:
            self.sender = profile
        if self.receiver_id == account_lineid:
            self.receiver = profile

        sender   = None
        receiver = None
        if isinstance(self.sender, models.Model):
            sender = self.sender
        elif self.sender:
            sender = self.sender.to_model(account)
        if isinstance(self.receiver, models.Model):
            receiver = self.receiver
        elif self.receiver:
            receiver = self.receiver.to_model(account)

        if self.toType == Message.GROUP:
            link_group = receiver
        elif self.toType == Message.ROOM:
            link_room = receiver
        elif self.toType == Message.USER:
            if not sender:
                print 'sender not found %s' % self.sender_id
            if account_lineid == sender_id:
                link_contact = receiver
            else:
                link_contact = sender
        else:  # Room, Not Implemented
            return None
            # raise NotImplementedError('Otherwise of message type (%s).', self.toType)
        message = Message(
            account=account,
            lineid=self.id,
            text=text,
            content_type=content_type,
            channel=self.toType,
            sender_id=sender_id,
            receiver_id=receiver_id,
            link_contact=link_contact,
            link_group=link_group,
            link_room=link_room,
            created_on=self.createdTime
        )
        with transaction.atomic():
            message.save()
            if content_preview:
                message.content_preview.save('%s.jpg' % self.id, File(open(content_preview, 'rb')))
            if content:
                message.content.save('%s.jpg' % self.id, File(open(content, 'rb')))
    return message
LineMessage.to_model = to_message_model
