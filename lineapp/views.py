# -*- coding: utf-8 -*-

import datetime
import time

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseNotFound
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt


from lineapp.models import Account, Contact, Group, Task, Message
from linedistribute.models import SiteConfiguration
from utils.decorators import json_response
from .forms import ImageForm


@login_required
def index(request):
    account = Account.objects.first()
    if not account:
        return redirect('/admin/lineapp/account/add/')
    assert account is not None, 'Line Account not found'
    assert account.profile is not None, 'Profile not found, Please launch line once.'

    config = SiteConfiguration.get_solo()
    contacts = account.contacts.exclude(pk=account.profile.pk).prefetch_unread_messages().order_by('-latest_message__created_on')
    if config.contact_limit:
        contacts = contacts.all()[:config.contact_limit]
    else:
        contacts = list(contacts.all())
    active_contacts = [x for x in contacts if any(x.unread_messages) or x.worked_by is not None]
    unread_contacts = [x for x in contacts if any(x.unread_messages)]
    if config.filter_active_contact:
        contacts = active_contacts
    notification_sound_url = None
    if config.notification_sound:
        notification_sound_url = config.notification_sound_url

    groups = account.groups.prefetch_unread_messages().order_by('-latest_message__created_on')
    if config.contact_limit:
        groups = groups.all()[:config.contact_limit]
    else:
        groups = list(groups.all())
    if config.filter_active_contact:
        groups = [x for x in groups if any(x.unread_messages) or x.worked_by is not None]

    rooms = account.rooms.prefetch_unread_messages().order_by('-latest_message__created_on')
    if config.contact_limit:
        rooms = rooms.all()[:config.contact_limit]
    else:
        rooms = list(rooms.all())
    if config.filter_active_contact:
        rooms = [x for x in rooms if any(x.unread_messages) or x.worked_by is not None]
    data = {
        'account': account,
        'contacts': contacts,
        'groups': groups,
        'rooms': rooms,
        'active_contacts': active_contacts,
        'unread_contacts': unread_contacts,
        'notification_sound_url': notification_sound_url,
    }
    return render(request, 'lineapp/index.html', data)


@login_required
@json_response
def contacts(request):
    account = Account.objects.first()
    assert account is not None, 'Line Account not found'
    assert account.profile is not None, 'Profile not found, Please launch line once.'

    config             = SiteConfiguration.get_solo()
    latest_message_id  = request.GET.get('latest_message_id', None)
    has_unread_message = False
    if latest_message_id:
        latest_message_id = int(latest_message_id)

    contacts = account.contacts.exclude(pk=account.profile.pk).prefetch_unread_messages().order_by('-latest_message__created_on')
    if config.contact_limit:
        contacts = contacts.all()[:config.contact_limit]
    else:
        contacts = list(contacts.all())
    active_contacts = [x for x in contacts if any(x.unread_messages) or x.worked_by is not None]
    if config.filter_active_contact:
        contacts = active_contacts

    groups = account.groups.prefetch_unread_messages().order_by('-latest_message__created_on')
    if config.contact_limit:
        groups = groups.all()[:config.contact_limit]
    else:
        groups = list(groups.all())
    if config.filter_active_contact:
        groups = [x for x in groups if any(x.unread_messages) or x.worked_by is not None]

    rooms = account.rooms.prefetch_unread_messages().order_by('-latest_message__created_on')
    if config.contact_limit:
        rooms = rooms.all()[:config.contact_limit]
    else:
        rooms = list(rooms.all())
    if config.filter_active_contact:
        rooms = [x for x in rooms if any(x.unread_messages) or x.worked_by is not None]

    for entity in list(contacts) + list(groups) + list(rooms):
        if has_unread_message:
            break
        for message in entity.unread_messages:
            if not latest_message_id or latest_message_id < message.pk:
                has_unread_message = True
                break

    for entity in list(contacts) + list(groups) + list(rooms):
        if entity.latest_message:
            if not latest_message_id or entity.latest_message.pk > latest_message_id:
                latest_message_id = entity.latest_message.pk

    contacts = [x.serialize() for x in contacts]
    groups = [x.serialize() for x in groups]
    rooms = [x.serialize() for x in rooms]

    data = {
        'is_online': account.is_online,
        'has_unread_message': has_unread_message,
        'latest_message_id': latest_message_id,
        'user_id': request.user.pk,
        'contacts': contacts,
        'groups': groups,
        'rooms': rooms,
    }
    return data


@login_required
def chat(request, lineid):
    account = Account.objects.first()
    assert account is not None
    profile = account.profile
    assert profile is not None
    account_lineid = profile.lineid
    contact = account.get_lineid(lineid)
    if not contact:
        raise HttpResponseNotFound()
    if contact.worked_by and contact.worked_by.pk != request.user.pk:  # Not authorize if chat opened from other
        data = {
            'contact': contact,
        }
        return render(request, 'lineapp/chat_deny.html', data)

    contact.worked_by        = request.user
    contact.latest_worked_by = request.user
    contact.worked_on        = datetime.datetime.now()
    contact.save(update_fields=['worked_by', 'worked_on', 'latest_worked_by'])

    # Delay 0.2 sec and check again for avoid collision
    time.sleep(0.2)
    contact = account.get_lineid(lineid)
    if contact.worked_by and contact.worked_by.pk != request.user.pk:  # Not authorize if chat opened from other
        data = {
            'contact': contact,
        }
        return render(request, 'lineapp/chat_deny.html', data)

    # Comment out for longpolling
    task = Task(account=account, action=Task.ACTION_GET_MESSAGES, args=dict(lineid=lineid, count=20))
    task.save()
    if not task.wait():
        print 'Task Timeout, skipped.'

    data = {
        'contact': contact,
        'account': account,
        'account_lineid': account_lineid,
    }
    return render(request, 'lineapp/chat.html', data)


@login_required
@json_response
def ajax_fetch_chat(request, lineid):
    latest_message_id = request.GET.get('latest_message_id', None)
    if latest_message_id:
        latest_message_id = int(latest_message_id)
    account = Account.objects.first()
    assert account is not None
    profile = account.profile
    assert profile is not None
    account_lineid = profile.lineid

    contact = account.get_lineid(lineid)
    if not contact:
        raise HttpResponseNotFound()
    # Update contact work before get message (decrease chance to error set job after closed job).
    contact.worked_on = datetime.datetime.now()
    if not contact.worked_by or contact.worked_by.pk != request.user.pk:
        contact.worked_by = request.user
        contact.latest_worked_by = request.user
    contact.save(update_fields=['worked_by', 'worked_on', 'latest_worked_by'])

    if not latest_message_id:
        messages = contact.get_messages(latest_message_id, timeout=30, limit=30, check_update_on_read=True)
    else:
        messages = contact.get_messages(latest_message_id, timeout=30, check_update_on_read=True)
        messages = list(messages)
    latest_line_read_message_id = contact.latest_line_read_message.pk if contact.latest_line_read_message else None

    ret_messages = []
    for message in messages:
        sender                       = message.sender()
        receiver                     = message.receiver()
        sender_name                  = sender.name if sender else None
        receiver_name                = receiver.name if receiver else None
        sender_profile_picture_url   = sender.profile_picture_url() if sender else None
        receiver_profile_picture_url = receiver.profile_picture_url() if receiver else None
        ready_by = str(message.read_by.nickname()) if message.read_by else None
        ret_messages.append({
            'id': message.pk,
            'text': message.text,
            'content_type': message.content_type,
            'content_preview': message.content_preview.url if message.content_preview else None,
            'content': message.content.url if message.content else None,
            'sender_id': message.sender_id,
            'receiver_id': message.receiver_id,
            'sender_name': sender_name,
            'receiver_name': receiver_name,
            'sender_profile_picture_url': sender_profile_picture_url,
            'receiver_profile_picture_url': receiver_profile_picture_url,
            'read_by': ready_by,
            'created_on': message.created_on.strftime('%d/%m/%y %H:%M'),
            'line_read': message.line_read,
        })
    data = {
        'account_lineid': account_lineid,
        'messages': ret_messages,
        'latest_line_read_message_id': latest_line_read_message_id,
    }
    return data


@login_required
@json_response
def ajax_fetch_history_chat(request, lineid):
    first_message_id = int(request.GET.get('first_message_id', None))
    account = Account.objects.first()
    assert account is not None
    profile = account.profile
    assert profile is not None
    account_lineid = profile.lineid

    contact = account.get_lineid(lineid)
    if not contact:
        raise HttpResponseNotFound()

    messages = contact.get_history_messages(first_message_id, limit=30)
    ret_messages = []
    for message in messages:
        sender                       = message.sender()
        receiver                     = message.receiver()
        sender_name                  = sender.name if sender else None
        receiver_name                = receiver.name if receiver else None
        sender_profile_picture_url   = sender.profile_picture_url() if sender else None
        receiver_profile_picture_url = receiver.profile_picture_url() if receiver else None
        ready_by = str(message.read_by.nickname()) if message.read_by else None
        ret_messages.append({
            'id': message.pk,
            'text': message.text,
            'content_type': message.content_type,
            'content_preview': message.content_preview.url if message.content_preview else None,
            'content': message.content.url if message.content else None,
            'sender_id': message.sender_id,
            'receiver_id': message.receiver_id,
            'sender_name': sender_name,
            'receiver_name': receiver_name,
            'sender_profile_picture_url': sender_profile_picture_url,
            'receiver_profile_picture_url': receiver_profile_picture_url,
            'read_by': ready_by,
            'created_on': message.created_on.strftime('%d/%m/%y %H:%M'),
            'line_read': message.line_read,
        })
    data = {
        'account_lineid': account_lineid,
        'messages': ret_messages,
    }
    return data


@login_required
@json_response
def ajax_mark_read_chat(request, lineid):
    config = SiteConfiguration.get_solo()
    read_message_id = request.GET.get('read_message_id', None)
    assert read_message_id is not None, 'Parameter `read_message_id` Not Found'
    read_message_id = int(read_message_id)
    account = Account.objects.first()
    assert account is not None
    profile = account.profile
    assert profile is not None

    contact = account.get_lineid(lineid)
    if not contact:
        raise HttpResponseNotFound()

    '''
    Prevent Deadlock
    -----
    Original style is
    result = contact.messages.filter(pk__lte=read_message_id, read_by__isnull=True).update(read_by=request.user)
    '''
    messages = contact.messages.filter(pk__lte=read_message_id, read_by__isnull=True)
    messages = list(messages.values_list('pk', flat=True))
    messages.sort()
    messages = Message.objects.filter(pk__in=messages)
    result = messages.update(read_by=request.user)
    '''
    End transaction
    '''
    if messages and config.enable_read_message:
        message = messages.order_by('-created_on').first()
        message.send_read()

    data = {
        'success': True,
        'messages': result,
    }
    return data


@login_required
@json_response
def ajax_send_message(request, lineid):
    message = request.GET.get('message', None)
    if not message:
        config = SiteConfiguration.get_solo()
        if not config.shortcut_message:
            return dict(success=False)
        else:
            message = config.shortcut_message
    account = Account.objects.first()
    contact = account.get_lineid(lineid)
    if not contact:
        raise HttpResponseNotFound()
    task = Task(account=account, action=Task.ACTION_SEND_MESSAGE, args=dict(lineid=lineid, message=message))
    task.save()
    if not task.wait():
        print 'Task Timeout, skipped.'
        task.set_complete()
        return dict(success=False)
    return dict(success=True)


@login_required
@json_response
def ajax_close_chat(request, lineid):
    account = Account.objects.first()
    contact = account.get_lineid(lineid)
    data = {
        'success': False
    }
    if not contact:
        return data
    if contact.worked_by and (contact.worked_by.pk == request.user.pk or request.user.is_superuser):
        contact.worked_by = None
        contact.save(update_fields=['worked_by'])
        data['success'] = True
    else:
        data['success'] = False
    return data


@login_required
@json_response
def ajax_find_contact_by_userid(request):
    line_userid = request.GET.get('line_userid', None)
    if not line_userid:
        return dict(success=False, message='userid not found.')
    account = Account.objects.first()
    task = Task(account=account, action=Task.ACTION_FIND_CONTACT_BY_USERID, args=dict(userid=line_userid))
    task.save()
    if not task.wait():
        print 'Task Timeout, skipped.'
        task.set_complete()
        return dict(success=False, message='timeout.')
    if not task.output['success']:
        return dict(success=False, message=task.output['message'])
    contact_id = task.output['id']
    source     = task.output['source']
    contact = Contact.objects.get(pk=contact_id)
    data = {
        'success': True,
        'contact': {
            'id': contact.pk,
            'name': contact.name,
            'status_message': contact.status_message,
            'profile_picture_url': contact.profile_picture_url('large'),
            'source': source,
        }
    }
    return data


@login_required
@json_response
def ajax_add_contact_by_userid(request):
    line_userid = request.GET.get('line_userid', None)
    if not line_userid:
        return dict(success=False)
    account = Account.objects.first()
    task = Task(account=account, action=Task.ACTION_ADD_CONTACT_BY_USERID, args=dict(userid=line_userid))
    task.save()
    if not task.wait():
        task.set_complete()
        return dict(success=False)
    contact_id = task.output
    if not contact_id:
        return dict(success=False)
    contact = Contact.objects.get(pk=contact_id)
    data = {
        'success': True,
        'contact': {
            'id': contact.pk,
            'name': contact.name,
            'status_message': contact.status_message,
            'profile_picture_url': contact.profile_picture_url('large'),
        }
    }
    return data


@login_required
def blocked(request):
    account = Account.objects.first()
    task = Task(account=account, action=Task.ACTION_GET_BLOCKED_CONTACTS, args=dict())
    task.save()
    if not task.wait():
        task.set_complete()
        raise Exception('Task Timeout.')
    if task.output is None:
        raise Exception('Task error.')
    contacts = []
    for pk in task.output:
        contact = Contact.objects.filter(pk=pk).first()
        if contact:
            contacts.append(contact)
    data = {
        'account': account,
        'contacts': contacts,
    }
    return render(request, 'lineapp/blocked.html', data)


@csrf_exempt
@login_required
@json_response
def upload_image(request):
    line_userid = request.GET.get('line_userid', None)
    if not line_userid:
        return False
    form = ImageForm(request.POST, request.FILES)
    account = Account.objects.first()
    if not account:
        return False
    if form.is_valid():
        entity = form.save()
        task = Task(account=account, action=Task.ACTION_SEND_IMAGE, args=dict(lineid=line_userid, image_id=entity.pk))
        task.save()
        return True
    return False


@json_response
def api_statistic(request):
    today         = datetime.date.today()
    account       = Account.objects.first()
    profile       = account.profile
    messages      = Message.objects.filter(created_on__gte=today).count()
    sent_messages = Message.objects.filter(created_on__gte=today, sender_id=profile.lineid).count()
    return {
        'success': True,
        'data': {
            'total_messages': messages,
            'sent_messages': sent_messages,
            'received_messages': messages - sent_messages,
        }
    }
