from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _
from django import forms

from .models import ImageAttachment


class ImageForm(forms.ModelForm):

    class Meta:
        model = ImageAttachment
        fields = ('image', )

    def clean_image(self):
        CONTENT_TYPES = ['image', 'jpg', 'jpeg', 'png']
        MAX_UPLOAD_SIZE = 1024 * 1024  # 1 MB
        image = self.cleaned_data['image']
        content_type = image.content_type.split('/')[0]
        if content_type in CONTENT_TYPES:
            if image._size > MAX_UPLOAD_SIZE:
                raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(image._size)))
        else:
            raise forms.ValidationError(_('File type is not supported'))
        return image
