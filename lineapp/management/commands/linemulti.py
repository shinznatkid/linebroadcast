#!/usr/bin/python
# -*- coding: utf-8
from requests.packages import urllib3
from threading import Thread
from ssl import SSLError
import traceback
import requests
import logging
import thrift
import socket
import socks
import time
import sys

from django.core.management.base import BaseCommand
from django.db.utils import OperationalError
from django.conf import settings

from lineapp.line import Line
from lineapp.models import Account, AccountState
from lineapi.exceptions import AnotherLoginException, LineException, LoginFailedException
from linedistribute.models import SiteConfiguration, Proxy


if getattr(settings, 'RAVEN_DSN', False):
    from raven.contrib.django.raven_compat.models import client

try:
    from django.db import close_old_connections
except ImportError:
    # Django < 1.8
    from django.db import close_connection as close_old_connections


def run_thread(account):
    print 'Run thread %s' % account.pk
    line_run = LineRun(account)

    while True:
        line_run.account.reload()  # Reload เอาค่า state
        verify = line_run.account.is_verified()

        if not verify:
            continue

        try:
            if verify and not line_run.line_main:  # เริ่มโปรแกรมใหม่ / เพิ่ง Verify ได้ ให้สร้าง Thread ใหม่ขึ้นมา
                line_run.start_line()
            elif verify and line_run.line_main and line_run.line_main.finished:  # line_main ตาย -> ชุบ
                line_run.start_line()

            if line_run.thread_error:  # เจอ Error แปลกๆ หยุดโปรแกรมเลย
                thread_error_raise = line_run.thread_error
                raise thread_error_raise[0], thread_error_raise[1], thread_error_raise[2]
        # except thrift.Thrift.TException as e:
        #     line_run.logger.warning('ThriftException (%s)!, Retry again in 10 sec.' % e)
        #     line_run.restart(delay=10)
        except OperationalError as e:
            line_run.logger.warning('MySQL server has gone away, Retry again in 10 sec.')
            close_old_connections()
            line_run.restart(delay=10)
        except LineException:
            if getattr(settings, 'RAVEN_DSN', False):
                client.captureException()
            line_run.logger.warning('LineException, retry in 10 sec.')
            line_run.restart(delay=10)
        except EOFError:
            line_run.logger.warning('EOF Error, restart in 10 sec.')
            line_run.restart(delay=10)
        except KeyboardInterrupt:
            import traceback
            traceback.print_exc()
            return
        except:
            raise
        time.sleep(1)


def create_thread(account):
    thread_name = 'Thread-Account%s' % account.pk
    thread = Thread(name=thread_name, target=run_thread, args=(account,))
    thread.setDaemon(True)
    thread.start()
    return thread


class LineRun(object):

    def __init__(self, account):
        self.account      = account
        self.logger       = logging.getLogger('line')
        self.line_main    = None
        self.thread_error = None

    def restart(self, delay=0):
        self.line_main = None
        if delay:
            time.sleep(delay)

    def start_line(self, delay=0):

        if delay:
            time.sleep(delay)
        self.account = self.account.reload()
        self.logger.info('Starting LINE (pk=%s, email=%s).' % (self.account.pk, self.account.email))
        try:
            self.line_main = Line(self.account)
            self.line_main.finished = False
        except requests.exceptions.RequestException as e:  # ไม่สำเร็จ รอครั้งใหม่
            self.logger.debug('Error: %s' % e)
            return
        except LoginFailedException as e:
            self.account.state = AccountState.UNVERIFY
            self.account.save(update_fields=['state'])
            self.logger.debug('LoginFailed: %s' % e)
            return
        except OperationalError as e:
            if 'database is locked' in e.message:
                self.logger.debug('Found Database Locked: %s' % e)
            else:
                self.logger.debug('OperationalError: %s' % e.message)
            return
        except Exception:
            if getattr(settings, 'RAVEN_DSN', False):
                client.captureException()
            raise

        while True:
            try:
                self.line_main.run()  # Loop
                self.logger.info('Line not continue.')
                self.line_main.finished = True
                break
            except AnotherLoginException as e:
                self.logger.warning(e)
                self.line_main.finished = True
                break
            except SSLError as e:
                self.logger.warning('SSLError (%s), Retry again in 10 sec.' % e)
                time.sleep(10)
                continue
            except (socket.timeout, socket.error, OSError) as e:
                self.logger.warning('Socket timeout (%s), Retry again in 10 sec.' % e)
                time.sleep(10)
                continue
            except requests.exceptions.ConnectionError as e:
                if e.message == 'Connection aborted.':
                    self.logger.warning('Line might be banned. Retry again in 1 min (Connection Aborted.)' % e)
                    time.sleep(60)
                    continue
                else:
                    self.logger.warning('Connection error (%s)!, Retry again in 10 sec.' % e)
                    time.sleep(10)
                    continue
            except requests.exceptions.RequestException as e:
                self.logger.warning('RequestException (%s)!, Retry again in 10 sec.' % e)
                time.sleep(10)
                continue
            except urllib3.exceptions.ReadTimeoutError as e:
                if getattr(settings, 'RAVEN_DSN', False):
                    client.captureException()
                self.logger.warning('ReadTimeoutError (might be banned#2), retry in 60 sec.')
                time.sleep(60)
                continue
            except urllib3.exceptions.HTTPError as e:
                if getattr(settings, 'RAVEN_DSN', False):
                    client.captureException()
                self.logger.warning(traceback.format_exc())
                self.logger.warning('HTTPError, retry in 10 sec.')
                time.sleep(10)
                continue
            except Exception as e:  # TODO: Error ที่ไม่จำเป็นต้อง Login ใหม่ ให้เอามาใส่ตรงนี้
                if getattr(settings, 'RAVEN_DSN', False):
                    client.captureException()
                self.thread_error = sys.exc_info()
                break

        if not self.line_main.finished:
            self.logger.info('Line command terminated.')


class Command(BaseCommand):

    help = 'Run Multiple Line accounts in same process (Proxy not supported)'

    def handle(self, *args, **options):

        threads = {}
        while True:
            accounts = Account.objects.all()  # .filter(state=AccountState.VERIFIED)

            for account in accounts:
                if account.pk not in threads:
                    thread = create_thread(account)
                    threads[account.pk] = thread
                    time.sleep(3)  # ต้องรอให้ตัวแรก login เสร็จก่อน
                else:
                    if not threads[account.pk].is_alive():  # ออกเลย
                        return
            time.sleep(1)

        # linerun = LineRun(args)
        # linerun.run()
        self.stdout.write('Successfully')
