#!/usr/bin/python
# -*- coding: utf-8
from requests.packages import urllib3
from threading import Thread
from ssl import SSLError
import traceback
import requests
import logging
import thrift
import socket
import socks
import time
import sys

from django.core.management.base import BaseCommand
from django.db.utils import OperationalError
from django.conf import settings

from lineapp.line import Line
from lineapp.models import Account
from lineapi.exceptions import AnotherLoginException, LineException
from linedistribute.models import SiteConfiguration, Proxy


if getattr(settings, 'RAVEN_DSN', False):
    from raven.contrib.django.raven_compat.models import client

try:
    from django.db import close_old_connections
except ImportError:
    # Django < 1.8
    from django.db import close_connection as close_old_connections


class LineRun(object):

    def __init__(self, args):
        self.logger  = logging.getLogger('line')
        self.account = self.wait_get_account(args)
        self.thread  = None

    def wait_get_account(self, args):
        if len(args) == 1:
            return Account.objects.filter(email__iexact=args[0]).get()
        else:
            while True:
                account = Account.objects.first()
                if account:
                    return account
                self.logger.debug('Line account not found, sleep.')
                time.sleep(10)

    def close_thread(self, min_sleep=0):
        if min_sleep:
            time.sleep(min_sleep)
        while self.thread and self.thread.is_alive():
            print 'Wait close.'
            time.sleep(1)
        self.line_main = None

    def run(self):
        self.thread_error = None

        def line_thread_run(self, logger, line_main):
            try:
                line_main.run()
                logger.info('Line not continue.')
                Line.is_continue = False
            except AnotherLoginException as e:
                logger.warning(e)
                Line.is_continue = False
            except Exception as e:
                self.thread_error = sys.exc_info()
                # raise
            if not Line.is_continue:
                logger.info('Line command terminated.')

        self.line_main   = None  # Line(account)
        Line.is_continue = True
        proxies = Proxy.get_proxies()
        proxy = proxies.next()
        while True:
            try:
                if self.line_main:
                    self.thread = Thread(target=line_thread_run, args=(self, self.logger, self.line_main,))
                    self.thread.setDaemon(True)
                    self.thread.start()
                while self.thread and self.thread.is_alive():
                    time.sleep(1)
                if self.thread_error:
                    thread_error_raise = self.thread_error
                    self.thread_error = None
                    raise thread_error_raise[0], thread_error_raise[1], thread_error_raise[2]
                if not Line.is_continue:  # Exit
                    self.logger.info('Exit')
                    break
                site_config = SiteConfiguration.get_solo()
                if proxy:
                    self.logger.info('Set proxy to %s' % proxy)
                    proxy.set_proxy()
                self.line_main = Line(self.account)  # re-login new Line
                self.line_main.ENABLE_LONG_POLLING = site_config.enable_long_polling
            except SSLError as e:
                self.logger.warning('SSLError (%s), Retry again in 10 sec.' % e)
                self.close_thread(10)
            except (socket.timeout, socket.error, OSError) as e:
                self.logger.warning('Socket timeout (%s), Retry again in 10 sec.' % e)
                self.close_thread(10)
                continue
            except requests.exceptions.ConnectionError as e:
                if e.message == 'Connection aborted.':
                    self.logger.warning('Line might be banned. (Connection Aborted.)' % e)
                    proxy = proxies.next()
                    if proxy:
                        self.logger.warning('Changing Proxy to %s' % proxy)
                        proxy.set_proxy()
                    continue
                self.logger.warning('Connection error (%s)!, Retry again in 10 sec.' % e)
                self.close_thread(10)
                continue
            except thrift.Thrift.TException as e:
                self.logger.warning('ThriftException (%s)!, Retry again in 10 sec.' % e)
                self.close_thread(10)
                continue
            except requests.exceptions.RequestException as e:
                self.logger.warning('RequestException (%s)!, Retry again in 10 sec.' % e)
                self.close_thread(10)
                continue
            except OperationalError as e:
                self.logger.warning('MySQL server has gone away, Retry again in 10 sec.')
                close_old_connections()
                self.close_thread(10)
                continue
            except LineException as e:
                if getattr(settings, 'RAVEN_DSN', False):
                    with Proxy.no_proxy():
                        client.captureException()
                self.logger.warning('LineException, retry in 10 sec.')
                self.close_thread(10)
                continue
            except urllib3.exceptions.ReadTimeoutError as e:
                if proxy:
                    self.logger.info('[P] PROXY %s' % proxy)
                if getattr(settings, 'RAVEN_DSN', False):
                    with Proxy.no_proxy():
                        client.captureException()
                proxy = proxies.next()
                if proxy:
                    self.logger.warning('Changing Proxy to %s' % proxy)
                    proxy.set_proxy()
                self.logger.warning('ReadTimeoutError (might be banned#2), retry in 10 sec.')
                self.close_thread(10)
                continue
            except socks.ProxyError as e:
                if proxy:
                    self.logger.info('[P] PROXY %s' % proxy)
                self.logger.warning('ProxyError (%s)' % e)
                proxy = proxies.next()
                if proxy:
                    self.logger.warning('Changing Proxy to %s' % proxy)
                continue
            except urllib3.exceptions.HTTPError as e:
                if getattr(settings, 'RAVEN_DSN', False):
                    with Proxy.no_proxy():
                        client.captureException()
                self.logger.warning(traceback.format_exc())
                self.logger.warning('HTTPError, retry in 10 sec.')
                self.close_thread(10)
                continue
            except EOFError as e:
                self.logger.warning('EOF Error, restart.')
                self.close_thread(1)
                continue
            except KeyboardInterrupt:
                if proxy:
                    self.logger.info('[P] PROXY %s' % proxy)
                break
            except:
                if proxy:
                    self.logger.info('[P] PROXY %s' % proxy)
                raise


class Command(BaseCommand):

    help = 'Run Line with specific account (or first)'

    def handle(self, *args, **options):
        linerun = LineRun(args)
        linerun.run()
        self.stdout.write('Successfully')
