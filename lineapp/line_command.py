# -*- coding: utf-8 -*-
from lineapi import exceptions
from .models import ImageAttachment


class LineCommand(object):

    def get_messages(self, lineid, count=1):
        entity = self.getContactOrRoomOrGroupById(lineid)
        if entity:
            messages = entity.getRecentMessages(count)
            for message in messages:
                if message.text:
                    message.to_model(self.account)
        else:  # Try
            message_box = self.getMessageBox(lineid)  # message_box is string
            if not message_box:
                print 'Debug message not found'
                return []  # Not found!?
            messages = self.getRecentMessages(message_box, count)
            for message in messages:
                message.to_model(self.account)

    def send_message(self, lineid, message):
        entity = self.getContactOrRoomOrGroupById(lineid)
        if entity:
            if lineid == self.profile.id:  # Send to self
                entity = self.profile
            message = message.encode('utf-8')
            result = entity.sendMessage(message)
            return result.id
        # Not work
        else:  # Try
            if self.config.auto_add_contact:
                try:
                    entity = self.findAndAddContactsByMid(lineid)  # Auto add contact
                    message = message.encode('utf-8')
                    entity.sendMessage(message)
                    return result.id
                except:
                    print 'cannot add account from lineid, try send with _send_message'
                    try:
                        message = message.encode('utf-8')
                        self._send_message(lineid, message)
                        return result.id
                    except:
                        print 'Cannot send message#2, skipped'
            print 'send_message error: lineid not found (%s).' % lineid

    def send_image(self, lineid, image_id):
        image = ImageAttachment.objects.filter(pk=image_id).get()
        entity = self.getContactOrRoomOrGroupById(lineid)
        if entity:
            if lineid == self.profile.id:  # Send to self
                entity = self.profile
            entity.sendImage(image.image.path)
        # Not work
        else:  # Try
            print 'send_image error: lineid not found (%s).' % lineid
            if self.config.auto_add_contact:
                try:
                    entity = self.findAndAddContactsByMid(lineid)  # Auto add contact
                    entity.sendImage(image.image.path)
                except:
                    print 'cannot add account from lineid, skipped'

    def read_message(self, lineid, message_id):
        entity = self.getContactOrRoomOrGroupById(lineid)
        if entity:
            self._sendChatChecked(lineid, message_id)

    def find_contact_by_userid(self, userid):
        result = {
            'success': False,
            'message': '',
        }
        try:
            source = 'other'
            contact = self.findContactByUserid(userid)
            contact_model = contact.to_model(self.account)
            contact_check = self.getContactOrRoomOrGroupById(contact_model.lineid)
            if contact_check:
                source = 'contact'
            elif contact_model.lineid == self.account.profile.lineid:
                source = 'self'
            result.update({
                'success': True,
                'id': contact_model.pk,
                'source': source,
            })
            return result
        except exceptions.AddContactUserExistsException:
            return result
        except exceptions.ContactNotFoundException:
            result['message'] = 'Contact not found.'
            return result
        except exceptions.FindContactBlockedException:
            result['message'] = 'Blocked.'
            return result
        return result

    def add_contact_by_userid(self, userid):
        try:
            contact = self.findAndAddContactByUserid(userid)
            contact_model = contact.to_model(self.account)
            return contact_model.pk
        except exceptions.AddContactUserExistsException:
            return False
        except exceptions.ContactNotFoundException:
            return False
        return False

    def get_blocked_contacts(self):
        try:
            contacts = self.getBlockedContacts()
            rets = []
            for contact in contacts:
                contact_model = contact.to_model(self.account)
                rets.append(contact_model.pk)
            return rets
        except exceptions.AddContactUserExistsException:
            return False
        except exceptions.ContactNotFoundException:
            return False
        return False
