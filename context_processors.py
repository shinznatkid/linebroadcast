# -*- coding: utf-8 -*-
from linedistribute.models import SiteConfiguration
import version


def site_configuration(request):
    context = {}
    config = SiteConfiguration.get_solo()
    if 'config' not in request:
        context['config'] = config
    if 'version' not in request:
        context['version'] = version
    return context
