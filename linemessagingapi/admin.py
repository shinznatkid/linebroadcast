from django.contrib import admin
from .models import LineMessagingContact


@admin.register(LineMessagingContact)
class BotRoleAdmin(admin.ModelAdmin):
    list_display = ('name', 'contact_id', 'created_by', 'updated_on')
    readonly_fields = ('created_on', 'updated_on')
