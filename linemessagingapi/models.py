# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class LineMessagingContact(models.Model):

    class Meta:
        verbose_name = "Line Messaging Contact"
        verbose_name_plural = "Line Messaging Contacts"

    def __str__(self):
        return self.name

    name         = models.CharField(default='Untitled', max_length=50)
    contact_id   = models.CharField(max_length=50)
    access_token = models.TextField()
    created_on   = models.DateTimeField(auto_now_add=True)
    created_by   = models.ForeignKey('auth.User')
    updated_on   = models.DateTimeField(auto_now=True)

    def send_message(self, message):
        import requests
        access_token = self.access_token
        data = {
            'to': self.contact_id,
            'messages': [{
                'type': 'text',
                'text': message[:2000],  # not more than 2000
            }],
        }
        headers = {
            'Authorization': 'Bearer %s' % access_token,
            'Content-type': 'application/json',
        }
        r = requests.post('https://api.line.me/v2/bot/message/push', data=json.dumps(data), headers=headers)
        # assert r.status_code == 200
        return r  # {status, message}
