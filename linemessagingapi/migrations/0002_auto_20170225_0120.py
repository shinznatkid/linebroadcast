# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('linemessagingapi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linemessagingcontact',
            name='access_token',
            field=models.TextField(default='xxx'),
            preserve_default=False,
        ),
    ]
