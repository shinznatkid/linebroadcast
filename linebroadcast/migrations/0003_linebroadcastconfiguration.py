# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('linebroadcast', '0002_auto_20151124_2154'),
    ]

    operations = [
        migrations.CreateModel(
            name='LineBroadcastConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('password', models.CharField(max_length=32, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'LineBroadcast Configuration',
            },
        ),
    ]
