# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lineapp', '0040_loginhistory'),
    ]

    operations = [
        migrations.CreateModel(
            name='SendMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('receiver_id', models.CharField(db_index=True, max_length=33, null=True, blank=True)),
                ('text', models.TextField(blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField()),
                ('state', models.IntegerField(default=0)),
                ('account', models.ForeignKey(related_name='send_messages', to='lineapp.Account')),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('link_contact', models.ForeignKey(related_name='send_messages', blank=True, to='lineapp.Contact', null=True)),
                ('link_group', models.ForeignKey(related_name='send_messages', blank=True, to='lineapp.Group', null=True)),
                ('link_room', models.ForeignKey(related_name='send_messages', blank=True, to='lineapp.Room', null=True)),
            ],
            options={
                'verbose_name': 'SendMessage',
                'verbose_name_plural': 'SendMessages',
            },
        ),
    ]
