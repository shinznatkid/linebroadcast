#!/usr/bin/python
# -*- coding: utf-8

import logging
import imaplib
import email
import json
import time
from imaplib import IMAP4
from socket import error as SocketError

from django.core.management.base import BaseCommand

from lineapp.models import Contact, Group
from ...models import SendMessage, LineBroadcastConfiguration


def get_unread_mail(mail):
    result, data = mail.search(None, '(UNSEEN)')

    unread_msgs = data[0].split()
    # print 'You have %s unreads.' % len(unread_msgs)

    ids = data[0]  # data is a list.
    unread_msgs = ids.split()  # ids is a space separated string

    da = []
    for e_id in unread_msgs:
        message = None
        _, data = mail.fetch(e_id, '(RFC822)')
        da.append(data[0][1])
        payloads = email.message_from_string(data[0][1])
        subject = payloads['subject'].strip()
        for part in payloads.walk():
            if part.get_content_type() == 'text/plain':
                message = part.get_payload(decode=True).strip()
                yield (subject, message)
                break


class Command(BaseCommand):

    help = 'Run Linebroadcast task'

    def handle(self, *args, **options):
        logger = logging.getLogger('line.parsemail')
        while True:
            try:
                conf = LineBroadcastConfiguration.get_solo()  # reload
                if not conf.email or not conf.password:
                    logger.debug('No password configure, sleep 5 mins.')
                    time.sleep(60 * 5)
                    continue
                mail = imaplib.IMAP4_SSL('imap.gmail.com')
                mail.login(conf.email, conf.password)
                while True:
                    mail.list()
                    # Out: list of "folders" aka labels in gmail.
                    mail.select("inbox")  # connect to inbox.
                    for access_token, message in get_unread_mail(mail):
                        logger.info('Incoming mail.')
                        try:
                            assert access_token
                            assert message
                        except:
                            continue
                        contact = Contact.objects.filter(access_token=access_token).first() or Group.objects.filter(access_token=access_token).first()
                        if not contact:
                            continue
                        account = contact.account
                        if not account.is_verified():
                            continue

                        SendMessage.create_message(contact, message)
                    time.sleep(30)
            except SocketError:
                logger.warning('Socket error.')
            except IMAP4.error:
                logger.warning('Imap error.')
            time.sleep(60)
