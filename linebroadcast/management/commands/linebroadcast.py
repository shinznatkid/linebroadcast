#!/usr/bin/python
# -*- coding: utf-8

import datetime
import logging
import time

from django.core.management.base import BaseCommand
from django.utils import timezone

from ...line import LineBroadcast
from utils.redisqueue import RedisQueue
from lineapp.models import Account, AccountState, LoginHistory
from lineapi.exceptions import LoginExpiredException
from ...models import SendMessage, SendMessageState
import version


class LineBroadcastCommand(object):

    @staticmethod
    def verify_account(account, login_history_pk):
        try:
            login_history = LoginHistory.objects.get(pk=login_history_pk)
        except:
            return False
        linebroadcast = LineBroadcast(account)
        result = linebroadcast.login()
        if result['success']:
            login_history.state = AccountState.VERIFIED
            login_history.save(update_fields=['state'])
            linebroadcast.get_contacts()
            return True
        if 'pincode' in result:
            login_history.pincode = result['pincode']
            login_history.state   = AccountState.WAIT_PINCODE
            login_history.save(update_fields=['state', 'pincode'])
            if linebroadcast.login_pincode():
                login_history.state   = AccountState.VERIFIED
                account.auth_token    = linebroadcast.authToken
                login_history.save(update_fields=['state'])
            else:
                login_history.state   = AccountState.UNVERIFY
                login_history.save(update_fields=['state'])
            account.pincode = None
            account.save(update_fields=['pincode', 'auth_token'])
            linebroadcast.get_contacts()
            return True
        login_history.state = AccountState.UNVERIFY
        login_history.state_message = result['message'] if 'message' in result else ''
        login_history.save(update_fields=['state', 'state_message'])
        if login_history.state_message:
            print login_history.state_message  # TODO: Log
        return False

    @staticmethod
    def get_contacts(account):
        linebroadcast = LineBroadcast(account)
        result = linebroadcast.get_contacts()
        return result

    @staticmethod
    def send_message(account, send_message_id):  # lineid, message):
        # TODO: delay between send across group, contact
        entity = SendMessage.objects.filter(pk=send_message_id).first()
        if not entity:
            return False
        if entity.created_on < timezone.now() - datetime.timedelta(hours=3):
            return False
        # account = entity.account
        linebroadcast = LineBroadcast(account)
        try:
            result = linebroadcast.send_message(entity.receiver_id, entity.text)
            if result:
                entity.set_state(SendMessageState.SENT)
            else:
                entity.set_state(SendMessageState.FAILED)
        except:
            entity.set_state(SendMessageState.FAILED)
            raise
        return result


class Command(BaseCommand):

    help = 'Run Linebroadcast task'

    def handle(self, *args, **options):
        logger = logging.getLogger('line.linebroadcast')
        queue = RedisQueue('linebroadcast')
        logger.info('Linebroadcast task build %s' % version.build)
        i = 0
        while True:
            for result in queue.get_iter():
                command, pk, args = result[0], result[1], result[2:]
                account = Account.objects.filter(pk=pk).first()
                if not account:
                    logger.debug('no account found.')
                    continue
                try:
                    if command == 'verify_account':
                        if LineBroadcastCommand.verify_account(account, *args):
                            logger.debug('success.')
                        else:
                            logger.debug('failed.')
                    elif command == 'get_contacts':
                        if LineBroadcastCommand.get_contacts(account, *args):
                            logger.debug('success.')
                        else:
                            logger.debug('failed.')
                    elif command == 'send_message':
                        if LineBroadcastCommand.send_message(account, *args):
                            logger.debug('success.')
                        else:
                            logger.debug('failed.')
                except LoginExpiredException:
                    account.auth_token = None
                    account.save(update_fields=['auth_token'])
                    logger.debug('failed.')
            time.sleep(0.1)
            if i % 1000 == 0:
                logger.debug('.')
            i += 1
            if i >= 10000:
                i = 0
        self.stdout.write('Successfully')
