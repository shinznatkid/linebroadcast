# -*- coding: utf-8 -*-

import logging
import json
import re

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib import auth

from lineapp.models import Account, AccountState, Contact, Group
from utils.decorators import json_response
from utils.redisqueue import RedisQueue
from .forms import AddAccountForm
from .models import SendMessage
from linenotify.models import LineNotifyCode
from linemessagingapi.models import LineMessagingContact


def json_account_decorator(f):
    def wrapper(request, pk, *args, **kwargs):
        data = {
            'success': False,
        }
        account = Account.objects.filter(pk=pk).first()
        if not account:
            return data
        return f(request, account, *args, **kwargs)
    return wrapper


def json_authen_decorator(f):
    def wrapper(request, *args, **kwargs):
        data = {
            'success': False,
        }
        if not request.user.is_authenticated():
            return data
        return f(request, *args, **kwargs)
    return wrapper


def index(request):
    if request.user.is_authenticated():
        return redirect('manage_accounts')
    return render(request, 'linebroadcast/index.html')


@login_required
def manage_accounts(request):
    accounts = Account.objects.filter(created_by=request.user).all()
    codes = LineNotifyCode.objects.filter(created_by=request.user).all()
    data = {
        'accounts': accounts,
        'codes': codes,
    }
    return render(request, 'linebroadcast/manage_accounts.html', data)


@login_required
def manage_account(request, pk):
    account = get_object_or_404(Account, pk=pk, created_by=request.user)
    data = {
        'account': account,
    }
    return render(request, 'linebroadcast/manage_account.html', data)


@login_required
def messages_history(request, pk):
    account = get_object_or_404(Account, pk=pk, created_by=request.user)
    data = {
        'account': account,
        'send_messages': account.send_messages.order_by('-pk')[:100],
    }
    return render(request, 'linebroadcast/messages_history.html', data)


@login_required
def add_account(request):
    form = AddAccountForm()
    if request.method == 'POST':
        form = AddAccountForm(request.POST)
        if form.is_valid():
            entity = form.save(request)
            return redirect('verify_account', pk=entity.pk)
    data = {
        'form': form,
    }
    return render(request, 'linebroadcast/add_account.html', data)


@login_required
def verify_account(request, pk):
    account = get_object_or_404(Account, pk=pk, created_by=request.user)
    if account.status and account.is_verified():
        return redirect('manage_account', pk=pk)

    data = {
        'account': account,
        'login_histories': account.login_histories.order_by('-pk').all(),
    }
    return render(request, 'linebroadcast/verify_account.html', data)


@csrf_exempt
@json_response
@json_authen_decorator
@json_account_decorator
def api_verify_account(request, account):
    login_histories = []
    for login_history in account.login_histories.order_by('-pk').all():
        login_histories.append({
            'state': login_history.state,
            'state_display': login_history.get_state_display(),
            'created_on': login_history.created_on.strftime('%Y/%m/%d %H:%M'),
            'pincode': login_history.pincode,
            'state_message': login_history.state_message,
        })
    data = {
        'success': True,  # just requests success
        'state': account.state,
        'state_display': account.get_state_display(),
        'status': account.status,
        'pincode': account.pincode,
        'state_message': account.state_message,
        'login_histories': login_histories,
    }
    if request.method == 'GET':
        return data
    else:
        if account.status and account.state == AccountState.VERIFIED:
            return data
        elif account.state == AccountState.VERIFING:
            return data
        else:
            account.verify()
            return data
    return {
        'success': False,
    }


# @json_response
# @login_required
# def get_contacts(request, pk):
#     account = get_object_or_404(Account, pk=pk, created_by=request.user)
#     if account.status:
#         queue = RedisQueue('linebroadcast')
#         queue.put(['get_contacts', account.pk])
#     else:
#         return {
#             'success': False,
#         }

#     data = {
#         'success': True
#     }
#     return data


@csrf_exempt
@json_response
def contact_token(request):
    account_id = request.POST.get('account_id')
    lineid = request.POST.get('lineid')
    data = {
        'success': False,
    }
    if not request.user.is_authenticated():
        return data
    if not account_id:
        return data
    if not lineid:
        return data
    account = Account.objects.filter(pk=account_id).first()
    if not account:
        return data
    contact = account.get_lineid(lineid)
    if not contact:
        return data
    access_token = contact.generate_token()
    data = {
        'success': True,
        'access_token': access_token,
    }
    return data


@csrf_exempt
@json_response
def send_message(request):
    data = {
        'success': False
    }
    access_token = request.GET.get('access_token', None) or request.POST.get('access_token', None)
    message      = request.POST.get('message', None)
    if not access_token:
        data['message'] = 'Invalid access token.'
        return data
    if not message:
        data['message'] = 'Invalid message.'
        return data

    contact = Contact.objects.filter(access_token=access_token).first() \
        or Group.objects.filter(access_token=access_token).first() \
        or LineNotifyCode.objects.filter(access_token=access_token).first() \
        or LineMessagingContact.objects.filter(contact_id=access_token).first()

    if not contact:
        data['message'] = 'Invalid contact.'
        return data
    if isinstance(contact, LineNotifyCode):
        result = contact.send_message(message)
        data['success'] = (result['status'] == 200)
        data['state_display'] = ''
        data['message'] = result.get('message')
        return data
    elif isinstance(contact, LineMessagingContact):
        result = contact.send_message(message)
        data['success'] = (result.status_code == 200)
        data['state_display'] = ''
        data['message'] = ''
        return data
    else:
        account = contact.account
        if not account.is_verified():
            data['message'] = 'Account not verified.'
            return data

        SendMessage.create_message(contact, message)
        # queue = RedisQueue('linebroadcast')
        # queue.put(['send_message', contact.account.pk, contact.lineid, message])
        data['success'] = True
        data['state_display'] = account.get_state_display()
        return data


@csrf_exempt
@json_response
def api_rtm(request):
    data = {
        'success': False
    }
    access_token      = request.GET.get('access_token', None) or request.POST.get('access_token', None)
    latest_message_id = request.GET.get('latest_message', None) or request.POST.get('latest_message', None)

    if not access_token:
        data['message'] = 'Invalid access token.'
        return data

    contact = Contact.objects.filter(access_token=access_token).first() or Group.objects.filter(access_token=access_token).first()
    if not contact:
        data['message'] = 'Invalid access token.'
        return data
    # account = contact.account
    # if not account.is_verified():
    #     data['message'] = 'Account not verified.'
    #     return data

    if latest_message_id:
        raw_messages = contact.messages.filter(pk__gt=latest_message_id).all()[:5]  # Latest 5 messages
    else:
        message = contact.messages.last()
        if message:
            raw_messages = [message]
        else:
            raw_messages = []

    messages = []
    for message in raw_messages:
        sender = message.sender()
        if sender:
            sender = {
                'name': sender.name,
            }
        messages.append({
            'id': message.pk,
            'text': message.text,
            'sender': sender,
            'created_on': message.created_on.strftime('%Y-%m-%d %H:%M:%S'),
        })

    data['success'] = True
    data['messages'] = messages
    return data


@csrf_exempt
@json_response
@json_authen_decorator
@json_account_decorator
def get_contacts(request, account):
    data = {
        'success': False
    }
    queue = RedisQueue('linebroadcast')
    queue.put(['get_contacts', account.pk])
    data['success'] = True
    return data


@login_required
def developer(request):
    return render(request, 'linebroadcast/developer.html')


@csrf_exempt
@json_response
def api_login(request):
    from tokenapi.tokens import token_generator

    if request.method == 'POST' and request.META['CONTENT_TYPE'].startswith('application/json'):
        request.POST = json.loads(request.body)

    username = request.POST.get('username', '')
    password = request.POST.get('password', '')

    user = auth.authenticate(username=username, password=password)

    if user is not None:
        if not user.is_active:
            return {'success': False, 'message': 'User account is disabled.'}
        auth.login(request, user)
        return {
            'success': True,
            'token': token_generator.make_token(user),
        }
    else:
        return {'success': False, 'message': 'Invalid username or password.'}


@csrf_exempt
@json_response
def sentry_webhook(request):
    data = {
        'success': False
    }
    access_token = request.GET.get('access_token', None)
    try:
        try:
            json_data = request.body
            json_data = json.loads(json_data)
        except:
            json_data = {
                'project_name': request.GET['project_name'],  # for test
                'message': request.GET['message'],  # for test
                'url': request.GET['url'],  # for test
            }
        message = 'Project %s\n%s\n%s' % (json_data['project_name'], json_data['message'], json_data['url'])
    except:
        data['message'] = 'Invalid structure'
        return data

    if not access_token:
        data['message'] = 'Invalid access token.'
        return data
    if not message:
        data['message'] = 'Invalid message.'
        return data

    contact = Contact.objects.filter(access_token=access_token).first()\
        or Group.objects.filter(access_token=access_token).first()\
        or LineNotifyCode.objects.filter(access_token=access_token).first()\
        or LineMessagingContact.objects.filter(contact_id=access_token).first()
    if not contact:
        data['message'] = 'Invalid contact.'
        return data
    if isinstance(contact, LineNotifyCode):
        result = contact.send_message(message)
        data['success'] = (result['status'] == 200)
        data['state_display'] = ''
        data['message'] = result.get('message')
        return data
    elif isinstance(contact, LineMessagingContact):
        result = contact.send_message(message)
        data['success'] = (result.status_code == 200)
        data['state_display'] = ''
        data['message'] = ''
        return data
    else:
        account = contact.account
        if not account.is_verified():
            data['message'] = 'Account not verified.'
            return data

        SendMessage.create_message(contact, message)
        # queue = RedisQueue('linebroadcast')
        # queue.put(['send_message', contact.account.pk, contact.lineid, message])
        data['success'] = True
        data['state_display'] = account.get_state_display()
        return data


@csrf_exempt
@json_response
def jira_webhook(request):

    def parse_jira(json_data):
        if json_data['webhookEvent'] == 'jira:issue_created' and json_data['issue_event_type_name'] == 'issue_created':  # New task
            re_result   = re.match(r'^https://(?P<subdomain>\w+)\.atlassian\.net/', json_data['issue']['self'])
            link_domain = re_result.group('subdomain')
            link        = 'https://%s.atlassian.net/browse/%s' % (link_domain, json_data['issue']['key'])
            message     = u'@{user} created task "{task_name}"\n\n{link}'.format(
                user=json_data['user']['displayName'],
                task_name=json_data['issue']['fields']['summary'],
                link=link,
            )
            return message
        if json_data['webhookEvent'] == 'jira:issue_updated' and json_data['issue_event_type_name'] == 'issue_commented':  # Comment
            re_result   = re.match(r'^https://(?P<subdomain>\w+)\.atlassian\.net/', json_data['issue']['self'])
            link_domain = re_result.group('subdomain')
            link        = 'https://%s.atlassian.net/browse/%s' % (link_domain, json_data['issue']['key'])
            message     = u'@{user} comment to task "{task_name}"\n\n"{comment}"\n\n{link}'.format(
                user=json_data['user']['displayName'],
                task_name=json_data['issue']['fields']['summary'],
                link=link,
                comment=json_data['comment']['body'],
            )
            return message

    data = {
        'success': False
    }
    access_token = request.GET.get('access_token', None)
    try:
        try:
            request_body = request.body
            json_data    = json.loads(request_body)
        except:
            json_data = {
                'webhookEvent': request.GET.get('webhookEvent', None),  # for test
                'issue': json.loads(request.GET.get('issue', None)),  # for test
                'comment': request.GET.get('comment', None),  # for test
            }
        message = parse_jira(json_data)
        assert message
    except:
        if settings.RAVEN_DSN:
            logger = logging.getLogger('line')
            logger.warning(request_body)  # Log
            from raven.contrib.django.raven_compat.models import client as raven_client
            raven_client.captureException()
        data['message'] = 'Invalid structure'
        return data

    if not access_token:
        data['message'] = 'Invalid access token.'
        return data
    if not message:
        data['message'] = 'Invalid message.'
        return data

    contact = Contact.objects.filter(access_token=access_token).first()\
        or Group.objects.filter(access_token=access_token).first()\
        or LineNotifyCode.objects.filter(access_token=access_token).first()\
        or LineMessagingContact.objects.filter(contact_id=access_token).first()
    if not contact:
        data['message'] = 'Invalid contact.'
        return data
    if isinstance(contact, LineNotifyCode):
        result = contact.send_message(message)
        data['success'] = (result['status'] == 200)
        data['state_display'] = ''
        data['message'] = result.get('message')
        return data
    elif isinstance(contact, LineMessagingContact):
        result = contact.send_message(message)
        data['success'] = (result.status_code == 200)
        data['state_display'] = ''
        data['message'] = ''
        return data
    else:
        account = contact.account
        if not account.is_verified():
            data['message'] = 'Account not verified.'
            return data

        SendMessage.create_message(contact, message)
        # queue = RedisQueue('linebroadcast')
        # queue.put(['send_message', contact.account.pk, contact.lineid, message])
        data['success'] = True
        data['state_display'] = account.get_state_display()
        return data
