# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from solo.models import SingletonModel
from django_enumfield import enum

from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
from django.db import models

from lineapp.models import Account, Contact, Group, Room
from utils.redisqueue import RedisQueue


class SendMessageState(enum.Enum):
    WAITING = 0
    SENT    = 1
    FAILED  = 2


@python_2_unicode_compatible
class SendMessage(models.Model):

    class Meta:
        verbose_name = "SendMessage"
        verbose_name_plural = "SendMessages"

    def __str__(self):
        return self.text

    account     = models.ForeignKey(Account, related_name='send_messages')
    receiver_id = models.CharField(max_length=33, blank=True, null=True, db_index=True)
    text        = models.TextField(blank=True)
    created_on  = models.DateTimeField(auto_now_add=True)
    created_by  = models.ForeignKey(User)
    updated_on  = models.DateTimeField(auto_now=True)
    state       = enum.EnumField(SendMessageState, default=SendMessageState.WAITING)

    link_contact = models.ForeignKey(Contact, blank=True, null=True, related_name='send_messages')
    link_group   = models.ForeignKey(Group, blank=True, null=True, related_name='send_messages')
    link_room    = models.ForeignKey(Room, blank=True, null=True, related_name='send_messages')

    @property
    def link_entity(self):
        return self.link_contact or self.link_group or self.link_room

    @staticmethod
    def create_message(contact, text):
        link_contact = contact if isinstance(contact, Contact) else None
        link_group   = contact if isinstance(contact, Group) else None
        link_room    = contact if isinstance(contact, Room) else None
        entity       = SendMessage(
            account=contact.account,
            receiver_id=contact.lineid,
            text=text,
            created_by=contact.account.created_by,
            link_contact=link_contact,
            link_group=link_group,
            link_room=link_room,
        )
        entity.save()
        queue = RedisQueue('linebroadcast')
        queue.put(['send_message', contact.account.pk, entity.pk])

    def set_state(self, new_state):
        self.state = new_state
        self.save(update_fields=['state'])


@python_2_unicode_compatible
class LineBroadcastConfiguration(SingletonModel):
    '''
    For email
    '''

    def __str__(self):
        return 'LineBroadcast Configuration'

    class Meta:
        verbose_name = 'LineBroadcast Configuration'

    email = models.EmailField(blank=True, null=True)
    password = models.CharField(blank=True, null=True, max_length=32)
