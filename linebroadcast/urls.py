from django.conf.urls import patterns, url, include

from django.contrib import admin
admin.autodiscover()

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^manage_accounts/$', views.manage_accounts, name='manage_accounts'),
    url(r'^add_account/$', views.add_account, name='add_account'),
    url(r'^account/(?P<pk>\d+)/$', views.manage_account, name='manage_account'),
    url(r'^account/(?P<pk>\d+)/messages_history/$', views.messages_history, name='messages_history'),
    url(r'^account/(?P<pk>\d+)/verify/$', views.verify_account, name='verify_account'),

    url(r'^account/(?P<pk>\d+)/get_contacts/$', views.get_contacts, name='get_contacts'),  # API
    url(r'^api/contact_token/$', views.contact_token, name='contact_token'),
    url(r'^api/(?P<pk>\d+)/get_contacts/$', views.get_contacts, name='api.get_contacts'),
    url(r'^api/send_message/$', views.send_message, name='api.send_message'),
    url(r'^api/sentry/webhook/$', views.sentry_webhook, name='api.sentry_webhook'),
    url(r'^api/jira/webhook/$', views.jira_webhook, name='api.jira_webhook'),
    url(r'^api/account/(?P<pk>\d+)/verify/$', views.api_verify_account, name='api.api_verify_account'),
    url(r'^api/rtm/$', views.api_rtm, name='api_rtm'),

    url(r'^api/login/$', views.api_login, name='api.api_login'),
    url(r'^api/', include('tokenapi.urls')),
    url(r'^linenotify/', include('linenotify.urls', namespace='linenotify')),

    url(r'^developer/$', views.developer, name='developer'),
]
