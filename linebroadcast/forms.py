# -*- coding: utf-8 -*-

from django.forms import ModelForm, PasswordInput


from lineapp.models import Account


class AddAccountForm(ModelForm):

    class Meta:
        model = Account
        widgets = {
            'password': PasswordInput(),
        }
        fields = ('nickname', 'email', 'password')

    def save(self, request, commit=True, *args, **kwargs):
        ret = super(AddAccountForm, self).save(commit=False, *args, **kwargs)
        # if not ret.created_by:  # Add only when added or assigned
        ret.created_by = request.user
        if commit:
            ret.save()
        return ret
