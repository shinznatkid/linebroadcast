# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('linenotify', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LineNotifyConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('client_id', models.CharField(max_length=32, null=True, blank=True)),
                ('client_secret', models.CharField(max_length=64, null=True, blank=True)),
                ('domain_name', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Line Notify Configuration',
            },
        ),
        migrations.AlterField(
            model_name='linenotifycode',
            name='name',
            field=models.CharField(default='Untitled', max_length=50),
        ),
    ]
