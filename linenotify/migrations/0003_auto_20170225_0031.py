# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('linenotify', '0002_auto_20161115_0042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='linenotifyconfiguration',
            name='domain_name',
            field=models.CharField(max_length=64, null=True, verbose_name='callback url', blank=True),
        ),
    ]
