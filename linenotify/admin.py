from django.contrib import admin

from .models import LineNotifyCode, LineNotifyConfiguration
from solo.admin import SingletonModelAdmin

admin.site.register(LineNotifyCode)


class LineNotifyConfigurationAdmin(SingletonModelAdmin):
    pass

admin.site.register(LineNotifyConfiguration, LineNotifyConfigurationAdmin)
