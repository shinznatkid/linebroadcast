# -*- coding: utf-8 -*-

from django.forms import ModelForm, PasswordInput


from .models import LineNotifyCode


class LineNotifyCodeForm(ModelForm):

    class Meta:
        model = LineNotifyCode
        fields = ('name',)
