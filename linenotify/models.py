# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from solo.models import SingletonModel


@python_2_unicode_compatible
class LineNotifyCode(models.Model):

    class Meta:
        verbose_name = "Line Notify Code"
        verbose_name_plural = "Line Notify Codes"

    def __str__(self):
        return self.name

    name         = models.CharField(default='Untitled', max_length=50)
    code         = models.CharField(max_length=50)
    access_token = models.CharField(max_length=128, blank=True, null=True)
    created_on   = models.DateTimeField(auto_now_add=True)
    created_by   = models.ForeignKey('auth.User')
    updated_on   = models.DateTimeField(auto_now=True)

    def send_message(self, message):
        import requests
        access_token = self.access_token
        data = dict(message=message[:1000])  # not more than 1000
        headers = dict(Authorization='Bearer %s' % access_token)
        r = requests.post('https://notify-api.line.me/api/notify', data=data, headers=headers)
        # assert r.status_code == 200
        return r.json()  # {status, message}


@python_2_unicode_compatible
class LineNotifyConfiguration(SingletonModel):

    def __str__(self):
        return 'Line Notify Configuration'

    class Meta:
        verbose_name = 'Line Notify Configuration'

    client_id     = models.CharField(blank=True, null=True, max_length=32)
    client_secret = models.CharField(blank=True, null=True, max_length=64)
    domain_name   = models.CharField('callback url', blank=True, null=True, max_length=64)

    def get_redirect_uri(self):
        if self.domain_name:
            return '%s/linenotify/create_callback/' % (self.domain_name.rstrip('/'))
