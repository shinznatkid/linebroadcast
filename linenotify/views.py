# -*- coding: utf-8 -*-

import requests
import urllib

from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import UpdateView
from django.core.urlresolvers import reverse
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy

from .models import LineNotifyConfiguration, LineNotifyCode
from .forms import LineNotifyCodeForm


@login_required
def create(request):
    conf = LineNotifyConfiguration.get_solo()
    params = {
        'response_type': 'code',
        'client_id': conf.client_id,
        'redirect_uri': conf.get_redirect_uri(),
        'scope': 'notify',
        'state': 'test',
        'response_mode': 'form_post',
    }
    base_url = 'https://notify-bot.line.me/oauth/authorize'
    url = '%s?%s' % (base_url, urllib.urlencode(params))
    return redirect(url)


@csrf_exempt
@login_required
def create_callback(request):
    assert request.POST.get('state') == 'test'
    code = request.POST.get('code', '')
    assert code
    # get token
    conf = LineNotifyConfiguration.get_solo()
    data = {
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': conf.get_redirect_uri(),
        'client_id': conf.client_id,
        'client_secret': conf.client_secret,
    }
    r = requests.post('https://notify-bot.line.me/oauth/token', data)
    assert r.status_code == 200
    response_text = r.text  # for debug
    result_object = r.json()
    access_token = result_object['access_token']
    line_notify_code = LineNotifyCode(access_token=access_token, code=code, created_by=request.user)
    line_notify_code.save()
    return redirect('linenotify:edit_code', pk=line_notify_code.pk)


# @login_required
# def edit_code(request, pk):
#     notify_code = get_object_or_404(LineNotifyCode, pk=pk, created_by=request.user)
#     form = LineNotifyCodeForm
#     data = {
#         'notify_code': notify_code,
#     }
#     return render(request, 'linenotify/edit_code.html', data)


class EditCodeView(UpdateView):
    template_name = "linenotify/edit_code.html"
    form_class = LineNotifyCodeForm
    model = LineNotifyCode

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EditCodeView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse('linenotify:edit_code', kwargs={'pk': pk})

    def get_queryset(self):
        queryset = super(EditCodeView, self).get_queryset()
        return queryset.filter(created_by=self.request.user)


class CodeDelete(DeleteView):
    model = LineNotifyCode
    success_url = reverse_lazy('index')
