# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0041_account_certificate'),
    ]

    operations = [
        migrations.CreateModel(
            name='BotMessageHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lineid', models.CharField(unique=True, max_length=33, db_index=True)),
                ('text', models.TextField(null=True, blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Bot Message History',
                'verbose_name_plural': 'Bot Message Histories',
            },
        ),
        migrations.CreateModel(
            name='BotRole',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('allow_guest', models.BooleanField(default=False)),
                ('activate_word', models.CharField(default='.', max_length=16, blank=True)),
                ('api_url', models.URLField()),
                ('active', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('allow_contacts', models.ManyToManyField(related_name='botroles', to='lineapp.Contact', blank=True)),
                ('allow_groups', models.ManyToManyField(related_name='botroles', to='lineapp.Group', blank=True)),
            ],
            options={
                'verbose_name': 'Bot Role',
                'verbose_name_plural': 'Bot Roles',
            },
        ),
        migrations.AddField(
            model_name='botmessagehistory',
            name='botrole',
            field=models.ForeignKey(related_name='messages', to='linebot.BotRole'),
        ),
        migrations.AddField(
            model_name='botmessagehistory',
            name='sender',
            field=models.ForeignKey(related_name='bot_messages', to='lineapp.Contact'),
        ),
    ]
