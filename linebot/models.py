# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
import requests

from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from lineapp.models import Contact, Group


class BotAPICallback(object):

    def __init__(self, success, message):
        self.success = success
        self.message = message

    def __repr__(self):
        return '%s: %s' % (self.success, self.message)


@python_2_unicode_compatible
class BotRole(models.Model):

    class Meta:
        verbose_name = "Bot Role"
        verbose_name_plural = "Bot Roles"

    def __str__(self):
        return self.name

    name           = models.CharField(max_length=255, blank=True, null=True)
    allow_contacts = models.ManyToManyField(Contact, related_name='botroles', blank=True)
    allow_groups   = models.ManyToManyField(Group, related_name='botroles', blank=True)
    allow_guest    = models.BooleanField(default=False)  # Allow contact all?
    activate_word  = models.CharField(max_length=16, blank=True, default='.')
    api_url        = models.URLField()
    active         = models.BooleanField(default=True)
    created_on     = models.DateTimeField(auto_now_add=True)
    updated_on     = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_roles(contact, receiver):
        contact_roles = []
        group_roles   = []

        guest_roles   = list(BotRole.objects.filter(active=True, allow_guest=True).all())
        if isinstance(contact, Contact):
            contact_roles = list(contact.botroles.filter(active=True).all())
        if isinstance(receiver, Group):
            group_roles = list(receiver.botroles.filter(active=True).all())

        return guest_roles + contact_roles + group_roles

    def call_api(self, message, sender=None):
        data = {
            'message': message,
        }
        if sender:
            data['context'] = json.dumps(dict(username=sender.lineid, nickname=sender.name))
        try:
            result = requests.post(self.api_url, data, timeout=10)  # TODO: สร้าง Session ต่อ Contact เก็บเข้าฐานข้อมูล พร้อมเวลา expired
            try:
                result = result.json()
                return BotAPICallback(success=result.get('success', False), message=result.get('message', ''))
            except:
                return BotAPICallback(success=False, message='Cannot parse result.')
        except Exception as e:
                return BotAPICallback(success=False, message='%s' % e)


@python_2_unicode_compatible
class BotMessageHistory(models.Model):

    class Meta:
        verbose_name = "Bot Message History"
        verbose_name_plural = "Bot Message Histories"

    def __str__(self):
        return self.lineid

    botrole         = models.ForeignKey(BotRole, related_name='messages')
    lineid          = models.CharField(max_length=33, unique=True, db_index=True)
    text            = models.TextField(blank=True, null=True)
    sender          = models.ForeignKey(Contact, related_name='bot_messages')
    created_on      = models.DateTimeField(auto_now_add=True)
