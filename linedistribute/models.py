# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from solo.models import SingletonModel
from contextlib import contextmanager

from django.utils.encoding import python_2_unicode_compatible
from django.db import models


@python_2_unicode_compatible
class SiteConfiguration(SingletonModel):

    NOTIFICATION_LINE_LOW     = 'line_low'
    NOTIFICATION_LINE_HIGH    = 'line_high'
    NOTIFICATION_LINE_BELL    = 'line_bell'
    NOTIFICATION_OHOH         = 'ohoh'
    NOTIFICATION_OHOH2        = 'ohoh2'
    NOTIFICATION_GUITAR       = 'guitar'
    NOTIFICATION_FIRSTBLOOD   = 'firstblood'
    NOTIFICATION_DOUBLEKILL   = 'doublekill'
    NOTIFICATION_KILLINGSPREE = 'killingspree'
    NOTIFICATION_DOMINATING   = 'dominating'
    NOTIFICATION_MEGAKILL     = 'megakill'
    NOTIFICATION_UNSTOPPABLE  = 'unstoppable'
    NOTIFICATION_WICKEDSICK   = 'wickedstick'
    NOTIFICATION_MONSTERKILL  = 'monsterkill'
    NOTIFICATION_GODLIKE      = 'godlike'
    NOTIFICATION_HOLYSHIT     = 'holyshit'
    NOTIFICATION_CS1          = 'cs1'
    NOTIFICATION_CS2          = 'cs2'
    NOTIFICATION_CS3          = 'cs3'
    NOTIFICATION_CS4          = 'cs4'
    NOTIFICATION_CS5          = 'cs5'

    NOTIFICATION_CHOICES = (
        (NOTIFICATION_LINE_LOW, 'line_low'),
        (NOTIFICATION_LINE_HIGH, 'line_high'),
        (NOTIFICATION_LINE_BELL, 'line_bell'),
        (NOTIFICATION_OHOH, 'Oh-Oh'),
        (NOTIFICATION_OHOH2, 'Oh-Oh2'),
        (NOTIFICATION_GUITAR, 'Guitar'),
        (NOTIFICATION_FIRSTBLOOD, 'First blood'),
        (NOTIFICATION_DOUBLEKILL, 'Double kill'),
        (NOTIFICATION_KILLINGSPREE, 'Killing Spree'),
        (NOTIFICATION_DOMINATING, 'Dominating'),
        (NOTIFICATION_MEGAKILL, 'Mega kill'),
        (NOTIFICATION_UNSTOPPABLE, 'Unstoppable'),
        (NOTIFICATION_WICKEDSICK, 'Wicked sick'),
        (NOTIFICATION_MONSTERKILL, 'Monster kill'),
        (NOTIFICATION_GODLIKE, 'Godlike'),
        (NOTIFICATION_HOLYSHIT, 'Holyshit'),
        (NOTIFICATION_CS1, 'cs1'),
        (NOTIFICATION_CS2, 'cs2'),
        (NOTIFICATION_CS3, 'cs3'),
        (NOTIFICATION_CS4, 'cs4'),
        (NOTIFICATION_CS5, 'cs5'),
    )

    site_name             = models.CharField(max_length=255, default='LINE Distribute')
    display_contacts      = models.BooleanField(help_text='Display contacts to page', default=True)
    display_rooms         = models.BooleanField(help_text='Display rooms to page', default=True)
    display_groups        = models.BooleanField(help_text='Display groups to page', default=True)
    line_pc_name          = models.CharField(max_length=255, default='WINDOWS-PC')
    auto_add_contact      = models.BooleanField(default=True)
    auto_reload           = models.PositiveIntegerField(help_text='Auto reload in seconds (set null for not reload)', null=True, blank=True, default=10)
    contact_limit         = models.PositiveIntegerField(help_text='Limit contacts (set null for unlimit)', null=True, blank=True, default=None)
    filter_active_contact = models.BooleanField(help_text='Filter only active contact', default=False)
    show_contact_picture  = models.BooleanField(default=True)
    notification_sound    = models.CharField(help_text='Notification sound in index page (null for mute).', choices=NOTIFICATION_CHOICES, max_length=255, default=NOTIFICATION_LINE_LOW, null=True, blank=True)
    chat_font             = models.PositiveIntegerField(help_text='Font size in chat message (px)', default=12)
    enable_long_polling   = models.BooleanField(default=False)
    enable_read_message   = models.BooleanField(default=False)
    random_proxy          = models.BooleanField(default=False)
    initial_proxy         = models.ForeignKey('Proxy', null=True, blank=True, on_delete=models.SET_NULL)
    shortcut_message      = models.TextField(help_text='Send this message when enter with empty message.', default=None, null=True, blank=True)

    def __str__(self):
        return 'Site Configuration'

    def notification_sound_url(self):
        return '/static/sounds/%s.mp3' % self.notification_sound

    class Meta:
        verbose_name = 'Site Configuration'


class SelectableProxy(object):

    def __init__(self, proxies, is_random=False):
        self.proxies = proxies
        self.index = None
        self.is_random = is_random

    def next(self):
        import random
        if not self.proxies:
            return None
        if self.index is None:
            conf = SiteConfiguration.get_solo()
            if conf.initial_proxy:
                self.index = self.proxies.index(conf.initial_proxy)
            else:
                self.index = 0
        else:
            if self.is_random:
                self.index = random.randint(0, len(self.proxies) - 1)
            else:
                self.index += 1
        if self.index >= len(self.proxies):
            self.index = 0  # reset
        return self.proxies[self.index]


@python_2_unicode_compatible
class Proxy(models.Model):

    class Meta:
        verbose_name = "Proxy"
        verbose_name_plural = "Proxys"

    name    = models.CharField(blank=True, null=True, max_length=50)
    url     = models.CharField(help_text='socks5://user:pass@ip:port', max_length=255)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        if self.name:
            return self.name
        return self.url

    @staticmethod
    def get_proxies():
        conf = SiteConfiguration.get_solo()
        return SelectableProxy(list(Proxy.objects.filter(enabled=True).all()), is_random=conf.random_proxy)

    @property
    def proxy_host(self):
        return getattr(self, '_proxy_host', None)

    def set_proxy(self):
        import socket
        import socks
        import urlparse
        parsed = urlparse.urlparse(self.url)
        Proxy._proxy_host = '%s@%s:%s' % (parsed.username, parsed.hostname, parsed.port)
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, parsed.hostname, port=parsed.port, username=parsed.username, password=parsed.password)
        if not hasattr(Proxy, 'original_socket'):
            Proxy.original_socket = socket.socket
        socket.socket = socks.socksocket

    @staticmethod
    @contextmanager
    def no_proxy():
        import socket
        if not hasattr(Proxy, 'original_socket'):
            yield
            return
        proxy_socket = socket.socket
        socket.socket = Proxy.original_socket
        try:
            yield
        finally:
            socket.socket = proxy_socket
