from django.views.generic.base import RedirectView
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.conf import settings

from django.contrib import admin
admin.autodiscover()
if getattr(settings, 'SSO_URL', False):
    admin.site.login = login_required(admin.site.login)

urlpatterns = [
    # url(r'^$', 'lineapp.views.index', name='index'),

    url(r'^hijack/', include('hijack.urls')),
    url(r'^moderator/', include('moderator.urls')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^activity/$', 'activity.views.index', name='activity.index'),
    # url(r'^activity/show_all/$', 'activity.views.show_all', name='activity.show_all'),
    # url(r'^activity/', include('actstream.urls')),
    url(r'^', include('linebroadcast.urls')),

    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico', permanent=False))
]

urlpatterns += [
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
        'show_indexes': True
    })
]
