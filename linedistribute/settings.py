from .common_settings import *


INSTALLED_APPS += [
    'lineapp',
    'accounts',
    'moderator',
    'activity',
    'bootstrap3',
    'linedistribute',
    'solo',
    'linebroadcast',
    'linebot',
    'api',
    'linenotify',
    'linemessagingapi',
    # 'sslserver',
    # 'actstream',
]

CORS_ORIGIN_ALLOW_ALL = True

try:
    from configs import *
except ImportError:
    pass
