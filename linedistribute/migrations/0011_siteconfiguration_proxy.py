# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0010_siteconfiguration_enable_read_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='proxy',
            field=models.CharField(help_text='user:pass@ip:port', max_length=255, null=True, blank=True),
        ),
    ]
