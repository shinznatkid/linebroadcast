# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0014_siteconfiguration_random_proxy'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='initial_proxy',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='linedistribute.Proxy', null=True),
        ),
    ]
