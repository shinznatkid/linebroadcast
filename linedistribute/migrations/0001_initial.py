# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SiteConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site_name', models.CharField(default='LINE Distribute', max_length=255)),
                ('display_contacts', models.BooleanField(default=True, help_text='Display contacts to page')),
                ('display_rooms', models.BooleanField(default=True, help_text='Display rooms to page')),
                ('display_groups', models.BooleanField(default=True, help_text='Display groups to page')),
                ('line_pc_name', models.CharField(default='WINDOWS-PC', max_length=255)),
                ('auto_add_contact', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Site Configuration',
            },
            bases=(models.Model,),
        ),
    ]
