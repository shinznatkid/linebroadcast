# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0011_siteconfiguration_proxy'),
    ]

    operations = [
        migrations.CreateModel(
            name='Proxy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('url', models.CharField(help_text='user:pass@ip:port', max_length=255)),
            ],
            options={
                'verbose_name': 'Proxy',
                'verbose_name_plural': 'Proxys',
            },
        ),
        migrations.RemoveField(
            model_name='siteconfiguration',
            name='proxy',
        ),
    ]
