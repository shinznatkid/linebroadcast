# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0013_auto_20150716_1232'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='random_proxy',
            field=models.BooleanField(default=False),
        ),
    ]
