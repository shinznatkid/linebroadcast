# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0004_siteconfiguration_show_contact_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='notification_sound',
            field=models.CharField(default='ohoh', choices=[('ohoh', 'Oh-Oh'), ('guitar', 'Guitar')], max_length=255, blank=True, help_text='Notification sound in index page (null for mute).', null=True),
            preserve_default=True,
        ),
    ]
