# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0012_auto_20150713_1318'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proxy',
            name='url',
            field=models.CharField(help_text='socks5://user:pass@ip:port', max_length=255),
        ),
    ]
