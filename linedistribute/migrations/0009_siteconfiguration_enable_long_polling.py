# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0008_auto_20150507_0105'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='enable_long_polling',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
