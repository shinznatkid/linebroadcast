# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0003_siteconfiguration_filter_active_contact'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='show_contact_picture',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
