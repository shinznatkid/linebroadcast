# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0016_proxy_enabled'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='shortcut_message',
            field=models.TextField(default=None, help_text='Send this message when enter with empty message.', null=True, blank=True),
        ),
    ]
