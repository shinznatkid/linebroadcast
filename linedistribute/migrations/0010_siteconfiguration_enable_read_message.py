# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0009_siteconfiguration_enable_long_polling'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='enable_read_message',
            field=models.BooleanField(default=False),
        ),
    ]
