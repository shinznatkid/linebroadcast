#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements. See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership. The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License. You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
#

import httplib
import requests
import os
import socket
import sys
import urllib
import urlparse
import warnings

from cStringIO import StringIO

from TTransport import *
from ..exceptions import ThriftException


class THttpClient(TTransportBase):
    """Http implementation of TTransport base."""

    def __init__(self, uri_or_host, port=None, path=None, proxies=None):
        """THttpClient supports two different types constructor parameters.

        THttpClient(host, port, path) - deprecated
        THttpClient(uri)

        Only the second supports https.
        """
        self.xls = None
        if port is not None:
            warnings.warn(
                "Please use the THttpClient('http://host:port/path') syntax",
                DeprecationWarning,
                stacklevel=2)
            self.host = uri_or_host
            self.port = port
            assert path
            self.path = path
            self.scheme = 'http'
        else:
            parsed = urlparse.urlparse(uri_or_host)
            self.scheme = parsed.scheme
            assert self.scheme in ('http', 'https')
            if self.scheme == 'http':
                self.port = parsed.port or httplib.HTTP_PORT
            elif self.scheme == 'https':
                self.port = parsed.port or httplib.HTTPS_PORT
            self.host = parsed.hostname
            self.path = parsed.path
            if parsed.query:
                self.path += '?%s' % parsed.query
        self.__wbuf = StringIO()
        self.__http = None
        self.__timeout = None
        self.__custom_headers = None
        self.__proxies = proxies
        self.__response = None

    def open(self):
        self.__http = requests.Session()

    def close(self):
        # if self.__response:
        #     self.__response.connection.close()
        # self.__http.close()
        # self.__http = None
        pass

    def isOpen(self):
        return self.__http is not None

    def setTimeout(self, ms):
        if ms is None:
            self.__timeout = None
        else:
            self.__timeout = ms / 1000.0

    def setCustomHeaders(self, headers):
        self.__custom_headers = headers

    def read(self, sz):
        if self.__response.headers:
            if 'x-ls' in self.__response.headers.keys():
                self.xls = self.__response.headers['x-ls']

        result = self.__response._content[:sz]
        self.__response._content = self.__response._content[sz:]
        return result
        # try:
        #     return next(self.__response.iter_content(sz))
        # except StopIteration:
        #     return ''

    def write(self, buf):
        self.__wbuf.write(buf)

    def flush(self):
        if self.isOpen():
            self.close()
        self.open()

        # Pull data out of buffer
        data = self.__wbuf.getvalue()
        self.__wbuf = StringIO()

        headers = {
            'Content-Type': 'application/x-thrift',
        }
        headers.update(self.__custom_headers)
        # if self.xls:
        #     headers['X-LS'] = self.xls
        uri = 'http://%s:%s%s' % (self.host, self.port, self.path)
        response = self.__http.post(uri, data=data, headers=headers, timeout=self.__timeout)  # , stream=True
        self.code = response.status_code
        self.headers = response.headers
        self.message = response.reason
        self.__response = response
        self.__response._content = response.content

    def __del__(self):
        if self.__response:
            self.__response.connection.close()
