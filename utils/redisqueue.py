# -*- coding: utf-8 -*-
import redis
import json

from django.conf import settings


class RedisQueue(object):
    """Simple Queue with Redis Backend"""
    def __init__(self, name, namespace='queue', **redis_kwargs):
        """The default connection parameters are: host='localhost', port=6379, db=0"""
        REDIS = getattr(settings, 'REDIS', {})
        REDIS.update(redis_kwargs)
        self.__db = redis.Redis(**REDIS)
        self.key  = '%s:%s' % (namespace, name)
        self._db = self.__db
        self.__db.get(None)  # get connection!

    def qsize(self):
        """Return the approximate size of the queue."""
        return self.__db.llen(self.key)

    def empty(self):
        """Return True if the queue is empty, False otherwise."""
        return self.qsize() == 0

    def put(self, item):
        """Put item into the queue."""
        self.__db.rpush(self.key, json.dumps(item))

    def get(self, block=True, timeout=None):
        """Remove and return an item from the queue.

        If optional args block is true and timeout is None (the default), block
        if necessary until an item is available."""
        if block:
            item = self.__db.blpop(self.key, timeout=timeout)
            if item:
                item = item[1]
        else:
            item = self.__db.lpop(self.key)

        if item:
            return json.loads(item)
        return item

    def get_nowait(self):
        """Equivalent to get(False)."""
        return self.get(False)

    def get_iter(self):
        while True:
            result = self.get_nowait()
            if result:
                yield result
                continue
            return
