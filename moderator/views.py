from django.shortcuts import render, get_object_or_404, redirect
from lineapp.models import Account, Contact, Group, Task, Message
from django.http import HttpResponseNotFound
from utils.decorators import json_response
from django.contrib.auth.decorators import login_required
from linedistribute.models import SiteConfiguration
from django.db import transaction
import datetime


@login_required
def index(request):
    account = Account.objects.first()
    assert account is not None, 'Line Account not found'
    contacts = list(account.contacts.exclude(pk=account.profile.pk).prefetch_unread_messages().prefetch_related('worked_by').order_by('-latest_message__created_on').all())
    groups = list(account.groups.exclude(pk=account.profile.pk).prefetch_unread_messages().prefetch_related('worked_by').order_by('-latest_message__created_on').all())
    rooms = list(account.rooms.exclude(pk=account.profile.pk).prefetch_unread_messages().prefetch_related('worked_by').order_by('-latest_message__created_on').all())
    data = {
        'account': account,
        'contacts': contacts,
        'groups': groups,
        'rooms': rooms,
    }
    return render(request, 'moderator/index.html', data)


@login_required
def chat(request, lineid):
    account = Account.objects.first()
    assert account is not None
    profile = account.profile
    assert profile is not None
    account_lineid = profile.lineid
    contact = account.get_lineid(lineid)
    if not contact:
        raise HttpResponseNotFound()
    task = Task(account=account, action=Task.ACTION_GET_MESSAGES, args=dict(lineid=lineid, count=20))
    task.save()
    if not task.wait():
        print 'Task Timeout, skipped.'
    data = {
        'contact': contact,
        'account_lineid': account_lineid,
    }
    return render(request, 'moderator/chat.html', data)


@login_required
@json_response
def ajax_fetch_chat(request, lineid):
    latest_message_id = request.GET.get('latest_message_id', None)
    if latest_message_id:
        latest_message_id = int(latest_message_id)
    account = Account.objects.first()
    assert account is not None
    profile = account.profile
    assert profile is not None
    account_lineid = profile.lineid

    contact = account.get_lineid(lineid)
    if not contact:
        raise HttpResponseNotFound()

    if not latest_message_id:
        messages = contact.get_messages(latest_message_id, timeout=30)  # Unlimited!
    else:
        messages = contact.get_messages(latest_message_id, timeout=30)
        messages = list(messages)

    ret_messages = []
    for message in messages:
        sender                       = message.sender()
        receiver                     = message.receiver()
        sender_name                  = sender.name if sender else None
        receiver_name                = receiver.name if receiver else None
        sender_profile_picture_url   = sender.profile_picture_url() if sender else None
        receiver_profile_picture_url = receiver.profile_picture_url() if receiver else None
        ready_by = str(message.read_by) if message.read_by else None
        ret_messages.append({
            'id': message.pk,
            'text': message.text,
            'content_type': message.content_type,
            'content_preview': message.content_preview.url if message.content_preview else None,
            'content': message.content.url if message.content else None,
            'sender_id': message.sender_id,
            'receiver_id': message.receiver_id,
            'sender_name': sender_name,
            'receiver_name': receiver_name,
            'sender_profile_picture_url': sender_profile_picture_url,
            'receiver_profile_picture_url': receiver_profile_picture_url,
            'read_by': ready_by,
        })
    data = {
        'account_lineid': account_lineid,
        'messages': ret_messages,
    }
    return data


def read_all(request):
    config = SiteConfiguration.get_solo()
    account = Account.objects.first()
    assert account is not None
    profile = account.profile
    assert profile is not None

    contacts = account.contacts.exclude(pk=account.profile.pk).prefetch_unread_messages().order_by('-latest_message__created_on')
    groups   = account.groups.exclude(pk=account.profile.pk).prefetch_unread_messages().order_by('-latest_message__created_on')
    rooms    = account.rooms.exclude(pk=account.profile.pk).prefetch_unread_messages().order_by('-latest_message__created_on')
    contacts = list(contacts) + list(groups) + list(rooms)

    for contact in contacts:
        '''
        Prevent Deadlock
        -----
        Original style is
        result = contact.messages.filter(read_by__isnull=True).update(read_by=request.user)
        '''
        # messages = contact.messages.filter(read_by__isnull=True)
        messages = contact.unread_messages
        messages = [x.pk for x in messages]
        messages.sort()
        messages = Message.objects.filter(pk__in=messages)
        messages.update(read_by=request.user)
        '''
        End transaction
        '''
        if messages and config.enable_read_message:
            message = messages.order_by('-created_on').first()
            message.send_read()
    return redirect('moderator.index')
