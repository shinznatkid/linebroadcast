from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    'moderator.views',
    url(r'^$', 'index', name='moderator.index'),
    url(r'^read_all$', 'read_all', name='moderator.read_all'),
    url(r'^chat/(?P<lineid>\w+)/$', 'chat', name='moderator.chat'),
    url(r'^chat/(?P<lineid>\w+)/fetch_chat/$', 'ajax_fetch_chat', name='moderator.ajax_fetch_chat'),
)
